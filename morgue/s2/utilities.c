/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: utilities.c 17 2007-07-11 10:00:16Z ni $
 *
 * Utility functions for Socket version 2.
 *
 */

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include "bsd_sysexits.h"
#include "socket2.h"

char utilities_ident[] = "$Id: utilities.c 17 2007-07-11 10:00:16Z ni $" ;


/** Write a message to standard error depending on the verbosity level.
 * 
 * @param level   Needed verbosity level for message.
 * @param format  Printf-like format string.
 * @param (format string arguments)
 */
void verbose(int level, char *format, ...)
{
    va_list arglist ;

/*     DEBUG_FUNCTION(("verbose(%d, %s, ...)", level, format)) ; */
    if (options.verbosity >= level) {
        va_start(arglist, format) ;
/*         fprintf(diag_out, PROGRAM_NAME ": ") ; */
        vfprintf(diag_out, format, arglist) ;
        va_end(arglist) ;
    }
}


/** Allocate memory; exit on failure.
 * 
 * @param nbytes    Amount of memory to allocate.
 * @param reason    Object of allocation (for informational purposes).
 * @return          Pointer to allocated area.
 */
void *ck_malloc(int nbytes, char *reason)
{
    void *space ;

    DEBUG_FUNCTION(("ck_malloc(%d, %s)", nbytes, reason)) ;
    space = malloc(nbytes) ;
    if (space == 0) {
	errno = ENOMEM ;
        syserr_exit(EX_OSERR, "cannot allocate %d bytes for %s",
		    nbytes, reason) ;
    }
    return space ;
}


/** Duplicate a string; exit on failure.
 *
 * @param string    The string to duplicate.
 * @param reason    Object of allocation (for informational purposes).
 * @return          Pointer to copy of string.
 */
char *ck_strdup(char *string, char *reason)
{
    char *space = ck_malloc(strlen(string) + 1, reason) ;

    DEBUG_FUNCTION(("ck_strdup(%s, %s)", string, reason)) ;
    return strcpy(space, string) ;
}



/** Check if `string' represents a decimal number.
 * 
 * @param string  The string to check.
 * @return        Non-zero if string consists of digits, zero else.
 */
int is_decimal(char *string)
{
    DEBUG_FUNCTION(("is_decimal(%s)", string)) ;
    while (*string) {
        if (!isdigit((unsigned char) *string)) {
            return 0 ;
        }
	string++ ;
    }
    return 1 ;
}


/** Expand LF characters in `src' to CRLF in `dest'. The space at
 * `dest' is supposed to be twice as large as `amount'.
 *
 * @param src     Pointer to data source.
 * @param amount  Amount of chars at `src'.
 * @param dest    Pointer to data destination.
 * @return        Number of characters at `dest'.
 */
int insert_crs(char *src, int amount, char *dest)
{
    int out_size ;              /* Resulting size. */

    DEBUG_FUNCTION(("insert_crs(src, %d, dest)", amount)) ;
    out_size = amount ;
    while (amount--) {
        if (*src == '\n') {
            *dest++ = '\r' ;
            out_size++ ;
        }
        *dest++ = *src++ ;
    }

    return out_size ;
}


/** Strip CR characters in `src' if followed by newlines, copying to `dest'.
 *
 * @param src     Pointer to data source.
 * @param amount  Amount of chars at `src'.
 * @param dest    Pointer to data destination.
 * @return        Number of characters at `dest'.
 */
int strip_crs(char *src, int amount, char *dest)
{
    int out_size ;

    DEBUG_FUNCTION(("strip_crs(src, %d, dest)", amount)) ;
    out_size = amount ;
    while (amount--) {
        if (*src == '\r' && amount && *(src + 1) == '\n') {
            src++ ;
            out_size-- ;
        } else {
            *dest++ = *src++ ;
        }
    }

    return out_size ;
}


#ifdef S2_DEBUG

FILE *s2_debug_stream ;

void s2dbg_initialize(void)
{
    s2_debug_stream = stderr ;
}

void s2_debug_printf(const char *format, ...)
{
    va_list arglist ;

/*     fprintf(s2_debug_stream, "s2_debug_printf(%s, ...)\n", format) ; */
    va_start(arglist, format) ;
    vfprintf(s2_debug_stream, format, arglist) ;
    va_end(arglist) ;
}

void s2dbg_print_iocall(char *call, int fd, int result)
{
    fprintf(s2_debug_stream, "called %5s on %d = %d",
	    call, fd, result) ;
}

#endif /* S2_DEBUG */


/* EOF */
