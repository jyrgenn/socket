/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: errors.c 8 2005-05-18 14:40:31Z ni $
 *
 * Error messages and error exits for Socket version 2.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "socket2.h"

char errors_ident[] = "$Id: errors.c 8 2005-05-18 14:40:31Z ni $" ;

int exit_dontflush = 0 ;
FILE *diag_out = 0 ;

static void err_vprintf(const char *format, va_list arglist)
{
    DEBUG_FUNCTION(("err_vprintf(%s, arglist)", format, arglist)) ;
    fprintf(diag_out, PROGRAM_NAME ": ") ;
    vfprintf(diag_out, format, arglist) ;
}

static void syserr_vprintf(const char *format, va_list arglist)
{
    DEBUG_FUNCTION(("syserr_vprintf(%s, arglist)", format, arglist)) ;
    err_vprintf(format, arglist) ;
    fprintf(diag_out, " (%s)\n", strerror(errno)) ;
}


/** Print a message about a system error (with help of strerror()).
 * 
 * @param format       format string
 * @param (optional format arguments)
 */
void syserr_printf(const char *format, ...)
{
    va_list arglist ;
    
    DEBUG_FUNCTION(("syserr_printf(%s, ...)", format)) ;
    va_start(arglist, format) ;
    syserr_vprintf(format, arglist) ;
    va_end(arglist) ;
}

/** Print a message about a system error (with help of strerror()) and
 * exit with the value of errno.
 *
 * @param exitcode    from sysexits
 * @param format      format string
 * @param (optional format arguments)
 */
void syserr_exit(int exitcode, char *format, ...)
{
    va_list arglist ;

    DEBUG_FUNCTION(("syserr_exit(%d, %s, ...)", exitcode, format)) ;
    va_start(arglist, format) ;
    syserr_vprintf(format, arglist) ;
    va_end(arglist) ;
    (exit_dontflush ? _exit : exit)(exitcode) ;
}


/** Print a message about an error.
 *
 * @param format       format string
 * @param (optional format arguments)
 */
void err_printf(char *format, ...)
{
    va_list arglist ;

    DEBUG_FUNCTION(("err_printf(%s, ...)", format)) ;
    va_start(arglist, format) ;
    err_vprintf(format, arglist) ;
    va_end(arglist) ;
    putc('\n', diag_out) ;
}

/** Print a message about an error and exit with value.
 *
 * @param exitcode     from sysexits
 * @param format       format string
 * @param (optional format arguments)
 */
void err_exit(int exitcode, char *format, ...)
{
    va_list arglist ;

    DEBUG_FUNCTION(("err_exit(%d, %s, ...)", exitcode, format)) ;
    va_start(arglist, format) ;
    err_vprintf(format, arglist) ;
    va_end(arglist) ;
    putc('\n', diag_out) ;
    (exit_dontflush ? _exit : exit)(exitcode) ;
}

/* EOF */
