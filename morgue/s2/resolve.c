/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: resolve.c 8 2005-05-18 14:40:31Z ni $
 * 
 * Resolver functions for Socket version 2.
 *
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>

#include "bsd_sysexits.h"
#include "socket2.h"

char resolve_ident[] = "$Id: resolve.c 8 2005-05-18 14:40:31Z ni $" ;


/** Resolve a hostname to an IP address and write the result to the
 * sockaddr_in passed. The hostname may also be an IP address in
 * dotted decimal notation.
 * 
 * @param hostname    hostname string
 * @param sinp        pointer to sockaddr_in
 * @return non-zero if the hostname could be resolved, zero else
 */
int resolve_hostname(char *hostname, struct sockaddr_in *sinp)
{
    struct hostent *h_ent ;

    DEBUG_FUNCTION(("resolve_hostname(%s, %p)", hostname, sinp)) ;
    memset(sinp, 0, sizeof(*sinp)) ;
    sinp->sin_family = AF_INET ;

    if (!inet_aton(hostname, &sinp->sin_addr)) {
        verbose(VERBOSE_NET, "looking up %s...", hostname) ;
        if ((h_ent = gethostbyname(hostname)) == NULL) {
            verbose(VERBOSE_NET, "not found\n") ;
            return 0 ;
        }
        memcpy(&sinp->sin_addr, h_ent->h_addr, h_ent->h_length) ;
        sinp->sin_family = h_ent->h_addrtype ;
        verbose(VERBOSE_NET, "found %s (%s)\n",
                h_ent->h_name, inet_ntoa(sinp->sin_addr)) ;
    }
    return 1 ;
}


/** Build a printable representation from a sockaddr_un struct and
 * return it. The returned string may not be modified.
 *
 * @param sa_un    the socket address struct
 * @return a static string with the printable representation
 */
static char *sockaddr_un_string(struct sockaddr_un *sa_un)
{
    DEBUG_FUNCTION(("sockaddr_un_string(%p)", sa_un)) ;
    return sa_un->sun_path[0] ? sa_un->sun_path : "LOCAL" ;
}


/** Build a printable representation from a sockaddr_in struct and
 * return it. Lookup hostname and service name, if possible. The
 * returned string is static and will be overwritten on the next
 * call.
 *
 * @param sa_in    the socket address struct
 * @return a static string with the printable representation
 */
static char *sockaddr_in_string(struct sockaddr_in *sa_in)
{
    static char string[MAX_HOSTNAME_LEN
		       + MAX_SERVICENAME_LEN
		       + sizeof(" (123.234.345.456:1234567890)")
		       + 1] ;	/* Space with some safety margin. */
    char servstring[sizeof("1234567890")] ;
    char *hostname ;		/* Name of host. */
    char *addr_string ;		/* String for IP address. */
    char *servicename ;		/* Name of service. */
    struct hostent *h_ent = 0 ;	/* Pointer to host entry struct. */
    struct servent *s_ent = 0 ;	/* Pointer to service entry struct. */

    DEBUG_FUNCTION(("sockaddr_in_string(%p)", sa_in)) ;
    addr_string = inet_ntoa(sa_in->sin_addr) ;

    if (!options.nolookups) {
	s_ent = getservbyport(sa_in->sin_port,
			     options.socktype_udp ? "udp" : "tcp") ;
	h_ent = gethostbyaddr((char *) &sa_in->sin_addr,
			     sizeof(struct in_addr), AF_INET) ;
    }
    if (s_ent) {
	servicename = s_ent->s_name ;
    } else {
	snprintf(servstring, sizeof(servstring), "%d",
		 ntohs(sa_in->sin_port)) ;
	servicename = servstring ;
    }
    if (h_ent) {
	hostname = h_ent->h_name ;
    } else {
	hostname = addr_string ;
    }
    if (h_ent || s_ent) {
	snprintf(string, sizeof(string), "%s:%s (%s:%u)",
		 hostname, servicename, addr_string, ntohs(sa_in->sin_port)) ;
    } else {
	snprintf(string, sizeof(string), "%s:%u",
		 addr_string, ntohs(sa_in->sin_port)) ;
    }
    return string ;
}


/** Build a printable representation from a sockaddr struct and
 * return it. The returned string may be static and may be
 * overwritten on the next call, and it may not be modified.
 *
 * @param saddr    the socket address struct
 * @return a static string with the printable representation
 */
char *sockaddr_string(struct sockaddr *saddr)
{
    DEBUG_FUNCTION(("sockaddr_string(%p)", saddr)) ;
    switch (saddr->sa_family) {
      case AF_INET:
	return sockaddr_in_string((struct sockaddr_in *) saddr) ;
#if defined(AF_UNIX) || defined(AF_LOCAL)
#ifdef AF_UNIX
      case AF_UNIX:
#else  /* AF_UNIX */
#ifdef AF_LOCAL
      case AF_LOCAL:
#endif /* AF_LOCAL */
#endif /* else AF_UNIX */
	return sockaddr_un_string((struct sockaddr_un *) saddr) ;
#endif /* defined(AF_UNIX) || defined(AF_LOCAL) */
      default:
	err_exit(EX_UNAVAILABLE, "cannot resolve address family %d",
		 saddr->sa_family) ;
	break ;
    }
    return 0 ;			/* Just to keep the compiler happy. */
}


/* Resolve the service name for protocol and return the port number in host
 * byte order. If the argument was a decimal number, return the number.
 *
 * @param name        port name string
 * @param protocol    "tcp" or "udp"
 * @return the port number if the port could be resolved, zero else
 */
int resolve_service(char *name, char *protocol)
{
    struct servent *servent ;

    DEBUG_FUNCTION(("resolve_service(%s, %s)", name, protocol)) ;
    if (is_decimal(name)) {
        return atoi(name) ;
    } else {
        verbose(VERBOSE_NET, "resolve service %s...", name) ;
        servent = getservbyname(name, protocol) ;
        if (servent == NULL) {
            verbose(VERBOSE_NET, "not found\n") ;
            return 0 ;
        } else {
            verbose(VERBOSE_NET, "found %s (%d)\n",
                    name, ntohs(servent->s_port)) ;
        }
        return ntohs(servent->s_port) ;
    }
}


/** Resolve host and service given in the form "host:service" to a
 * sockaddr_in struct. Host and service may also be specified
 * numerically, i. e. as a dotted decimal IP address and a port
 * number string. IP address and port number are written into the
 * sockaddr_in struct supplied by the caller.
 *
 * @param host_port   string with host and port specifier
 * @param sin_p       socket address struct to carry result
 * @param service_p   pointer to service variable; may be NULL
 * @return            zero iff successful
 */
int resolve_host_port(char *host_port, struct sockaddr_in *sin_p,
		      char **service_p, char *proto)
{
    char *host = host_port ;	/* Pointer to host part. */
    char *service ;		/* Pointer to service part. */
    unsigned short port ;	/* Port number. */

    DEBUG_FUNCTION(("resolve_host_port(%s, %p, %p, %s)",
		    host_port, sin_p, service_p, proto)) ;
    service = strchr(host_port, ':') ;
    if (service == 0) {
	return RESERR_INVALID ;
    }
    if (service == host) {
	*service++ = '\0' ;
	sin_p->sin_family = AF_INET ;
	sin_p->sin_addr.s_addr = INADDR_ANY ;
    } else {
	*service++ = '\0' ;
	if (!resolve_hostname(host, sin_p)) {
	    return RESERR_HOST ;
	}
    }

    if (service_p) {
	*service_p = service ;
    }
    
    port = resolve_service(service, proto) ;
    if (!port) {
	return RESERR_PORT ;
    }
    sin_p->sin_port = htons(port) ;

    return RESERR_OK ;
}

/* EOF */
