#!/usr/local/bin/perl -p

use warnings;
use strict;

sub bqnonl {
    my $result = `@_`;
    chomp($result);
    return $result;
}

our $cc;
BEGIN {
    $cc = shift() || die("usage: $0 cc-cmd\n");
}

s{%%__isodate__%%}{bqnonl("date +%Y-%m-%d_%H:%M:%S")}ge;
s{%%__build__%%}  {$cc};
s{%%__hostname__%%}{bqnonl("hostname")}ge ;
s{%%__platform__%%}{bqnonl("uname -m -r -s")}ge ;
s{%%__username__%%}{bqnonl("whoami")}ge ;
