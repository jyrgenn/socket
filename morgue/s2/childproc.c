/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: childproc.c 8 2005-05-18 14:40:31Z ni $
 *
 * child process setup for Socket version 2.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <errno.h>

#include "bsd_sysexits.h"
#include "socket2.h"
#include "selectloop.h"

char childproc_ident[] = "$Id: childproc.c 8 2005-05-18 14:40:31Z ni $" ;

/* Put a variable into the environment.
 */
static void putenv_copy_2(char *varname, char *value)
{
    char *envstr ;

    /* We need space for the strings, a '=', and the null byte. */
    envstr = ck_malloc(strlen(varname) + strlen(value) + 2, "putenv") ;
    strcpy(envstr, varname) ;
    strcat(envstr, "=") ;
    strcat(envstr, value) ;
    if (putenv(envstr) != 0) {
	syserr_printf("putenv %s", varname) ;
	return ;		/* simply fail here, that's ok. */
    }
}

static void putenv_addr(char *addrname, char *portname,
			struct sockaddr_in *sip)
{
    if (sip) {    
	char *addrstr ;
	char *portstr ;
	char *endstr ;

	addrstr = sockaddr_string((struct sockaddr *) sip) ;
	if ((portstr = strchr(addrstr, ':'))) {
	    *portstr++ = 0 ;
	    if ((endstr = strchr(portstr, ' '))) {
		*endstr = 0 ;
	    }	    
	    putenv_copy_2(addrname, addrstr) ;
	    putenv_copy_2(portname, portstr) ;
	}
    } else {
	DEBUG(S2_DBG_SYSERRS,
	      s2_debug_printf("getsock/peername(): %s", strerror(errno))) ;
	putenv_copy_2(addrname, "<local>") ;
	putenv_copy_2(portname, "0") ;
    }
}


/** Create a child process running the specified command line and connect its
 * input and outputs to the specified file descriptors. Put information about
 * local and remote address of the sepcified socket into the child's
 * environment (if it is a TCP or UDP socket at all). Return zero if
 * everything is fine, -1 otherwise. Set errno as appropriate.
 * 
 * @param command    command line to be run
 * @param sockfd     socket file descriptor (for getsock/peername())
 * @param in_fd      file descriptor to be connected to program's stdin
 * @param out_fd     file descriptor to be connected to program's stdout/stderr
 * @return -1 in case of an error, zero otherwise
 */
int connect_child_proc(char *command, int sockfd, int *in_fd, int *out_fd)
{
    int from_cld[2] ;		/* from child process */
    int to_cld[2] ;		/* to child process */
    struct sockaddr_in sa_in ;
    socklen_t addrlen ;

    /* set environment variables for child process for peer and
     * local address and port */
    addrlen = sizeof(sa_in) ;
    if (getpeername(sockfd, (struct sockaddr *) &sa_in, &addrlen) < 0) {
	putenv_addr(ENVNAME_PEERADDR, ENVNAME_PEERPORT, 0) ;
    } else {
	putenv_addr(ENVNAME_PEERADDR, ENVNAME_PEERPORT, &sa_in) ;
    }
    addrlen = sizeof(sa_in) ;
    if (getsockname(sockfd, (struct sockaddr *) &sa_in, &addrlen) < 0) {
	putenv_addr(ENVNAME_OWNADDR, ENVNAME_OWNPORT, 0) ;
    } else {
	putenv_addr(ENVNAME_OWNADDR, ENVNAME_OWNPORT, &sa_in) ;
    }

    /* create pipes */
    if (pipe(from_cld) == -1) {
	syserr_exit(EX_OSERR, "create pipe") ;
    }
    if (pipe(to_cld) == -1) {
	syserr_exit(EX_OSERR, "create pipe") ;
    }

    /* for child process */
    switch (fork()) {
      case 0:			/* this is the child process */
	/* connect stdin to pipe */
	close(0) ;
	close(to_cld[1]) ;
	dup2(to_cld[0], 0) ;
	close(to_cld[0]) ;
	/* connect stdout to pipe */
	close(1) ;
	close(from_cld[0]) ;
	dup2(from_cld[1], 1) ;
	/* connect stderr to pipe */
	close(2) ;
	dup2(from_cld[1], 2) ;
	close(from_cld[1]) ;
	/* call program via sh */
	execl("/bin/sh", "sh", "-c", command, NULL) ;
	syserr_printf("exec /bin/sh") ;
	/* terminate parent silently */
	kill(getppid(), SIGUSR1) ;
	exit(EX_OSFILE) ;
	break ;
      case -1:
	syserr_exit(EX_OSERR, "fork") ;
	break ;
      default:			/* parent process */
	*out_fd = from_cld[0] ;
	*in_fd = to_cld[1] ;
	break ;
    }
    return 0 ;
}

/* EOF */
