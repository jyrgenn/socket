/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: dgramio.c 8 2005-05-18 14:40:31Z ni $
 *
 * datagram I/O handlers for Socket version 2.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "bsd_sysexits.h"
#include "socket2.h"
#include "selectloop.h"

char dgramio_ident[] = "$Id: dgramio.c 8 2005-05-18 14:40:31Z ni $" ;

/* In order to maintain the packet boundaries in the data we write,
 * we store read data as a list of packets, which are then (if at
 * all possible) written out in a single packet. */
typedef struct DGENTRY {
    char *data ;                /* Data as read. */
    int size ;                  /* Allocated size of buffer. */
    int used ;                  /* Amount of data in buffer. */
    int written ;               /* Non-zero if not all data could be
                                 * written; indicates place to start
                                 * next write with. */
    struct DGENTRY *next ;	/* Next datagram entry in list. */
} dgentry_t ;


typedef struct DGRAMBUFFER {
    int is_from_socket ;	/* Non-zero if in_fd is the socket;
				 * otherwise it's out_fd. */
    int in_fd ;                 /* File descriptor to read from. */
    dgentry_t *first ;		/* First entry of datagram list. */
    dgentry_t *last ;		/* Last entry of datagram list. */
    int buffered ;		/* Total size of buffered data (tr'ed).
                                 * (if applicable, otherwise null). */
    LONGLONG total_read ;       /* Amount of data read from in_fd.
				 * Be aware that the LONGLONG will,
				 * at 1 GB/s, overflow after 272
				 * years. */
    LONGLONG in_packets ;	/* Number of packets  */
    int out_fd ;                /* File descriptor to write to. */
    LONGLONG total_written ;    /* Amount of data written to out_fd. */
} dgrambuffer_t ;

static dgrambuffer_t fromsock_buf, tosock_buf ;
static char *tr_data ;
static dgentry_t *dgfreelist = 0 ; /* Guess what this is. :-) */


static dgentry_t *new_dgentry(void)
{
    dgentry_t *newentry ;
    char *data ;

    DEBUG_FUNCTION(("new_dgentry()")) ;
    if (dgfreelist) {
	newentry = dgfreelist ;
	dgfreelist = dgfreelist->next ;
	data = newentry->data ;
    } else {
	newentry = ck_malloc(sizeof(dgentry_t), "dgram entry") ;
	data = ck_malloc((options.translate_lf ? 2 : 1) * options.buffersize,
			 "dgram entry data") ;
    }
    memset(newentry, 0, sizeof(*newentry)) ;
    newentry->data = data ;
    newentry->size = options.buffersize ;
    return newentry ;
}

static void free_dgentry(dgentry_t *entry)
{
    entry->next = dgfreelist ;
    dgfreelist = entry ;
}


static void write_handler(int fd, void *context)
{
    dgrambuffer_t *buf = context ;
    int nbytes ;

    DEBUG_FUNCTION(("dg write_handler(%d, %p)", fd, context)) ;
    reset_receive_timeout() ;
    if (buf->first) {
	dgentry_t *dgram = buf->first ;

	DEBUG(S2_DBG_IOCALLS,
	      s2_debug_printf("dgram->used = %d, dgram->written = %d",
			      dgram->used, dgram->written)) ;
	nbytes = write(buf->out_fd, dgram->data + dgram->written,
		       dgram->used - dgram->written) ;
	DEBUG(S2_DBG_IOCALLS,
	      s2dbg_print_iocall("write", buf->out_fd, nbytes)) ;
	if (nbytes == -1) {
	    syserr_exit(EX_IOERR, "error writing to %s",
			buf->is_from_socket ? "standard output" : "socket") ;
	}
	buf->total_written += nbytes ;
	dgram->written += nbytes ;
	buf->buffered -= nbytes ;

	if (!buf->is_from_socket) {
	    verbose(VERBOSE_MSGS, "<- wrote %d bytes to socket, %Ld total\n",
		    nbytes, buf->total_written) ;
	}
	
	if (dgram->written >= dgram->used) {
	    /* Dequeue packet and adjust read and write handlers
	       accordingly. */
	    buf->first = dgram->next ;
	    free_dgentry(dgram) ;
	    if (buf->first == NULL) {
		buf->last = 0 ;
		sl_suspend_fd_handler(buf->out_fd, SL_OUTPUT) ;
	    }
	}
	if (buf->buffered < options.buffersize) {
	    sl_resume_fd_handler(buf->in_fd, SL_INPUT) ;
	}
    } else {
	err_printf("dg write handler %s socket called without data",
		   buf->is_from_socket ? "from" : "to") ;
	sl_suspend_fd_handler(buf->out_fd, SL_OUTPUT) ;
    }
} 


static void read_handler(int fd, void *context)
{
    dgrambuffer_t *buf = context ;
    dgentry_t *dgram ;
    int nbytes ;

    DEBUG_FUNCTION(("dg read_handler(%d, %p)", fd, context)) ;
    reset_receive_timeout() ;
    if (buf->buffered < options.buffersize) {
	dgram = new_dgentry() ;
	if (buf->first) {
	    assert(buf->last) ;
	    buf->last->next = dgram ;
	} else {
	    buf->first = dgram ;
	}
	buf->last = dgram ;

	nbytes = read(buf->in_fd, dgram->data, dgram->size) ;
	DEBUG(S2_DBG_IOCALLS, s2dbg_print_iocall("read", buf->in_fd, nbytes)) ;
	switch (nbytes) {
	  case -1:			
	    /* Read error. */
	    syserr_exit(EX_IOERR, "error reading from %s",
			buf->is_from_socket ? "socket" : "standard input") ;
	    break ;                 /* For cosmetic raisins. */
	  case 0:
	    if (!buf->is_from_socket) {
		/* EOF on read. */
		verbose(VERBOSE_NET, "EOF on standard input\n") ;
		if (options.quit_on_eof) {
		    exit(0) ;
		} else {
		    /* Otherwise do half close and deinstall read handler. */
		    if (sl_remove_fd_handler(buf->in_fd, SL_INPUT) != 0) {
			syserr_exit(EX_SOFTWARE, "error unhandling %s fd %d",
				    buf->is_from_socket ? "socket" : "input",
				    buf->in_fd) ;
		    }
		    if (shutdown(buf->out_fd, SHUT_WR) != 0) {
			syserr_printf("half-close on socket failed") ;
		    }
		}
		close(buf->in_fd) ;
		break ;
	    }
	    /* FALLTHROUGH */
	  default:
	    /* Successful read. */
	    if (options.translate_lf) {
		char *tmpdata ;	/* For swapping with translated data. */

		if (buf->is_from_socket) {
		    nbytes = strip_crs(dgram->data, nbytes, tr_data) ;
		} else {
		    nbytes = insert_crs(dgram->data, nbytes, tr_data) ;
		}
		tmpdata = dgram->data ;
		dgram->data = tr_data ;
		tr_data = tmpdata ;
	    }
	    buf->total_read += nbytes ;
	    buf->buffered += nbytes ;
	    dgram->used = nbytes ;
	    if (buf->is_from_socket) {
		verbose(VERBOSE_MSGS,
			"-> read %d bytes from socket, %Ld total\n",
			nbytes, buf->total_read) ;
	    }
	    if (options.text_dump) {
		text_dump_packet(dgram->data, nbytes, buf->is_from_socket) ;
	    }
	    if (options.hex_dump) {
		hexdump_packet(dgram->data, nbytes, buf->is_from_socket) ;
	    }
	    if (buf->buffered >= options.buffersize) {
		sl_suspend_fd_handler(buf->in_fd, SL_INPUT) ;
	    }
	    sl_resume_fd_handler(buf->out_fd, SL_OUTPUT) ;
	}
    } else {
	err_printf("dg read handler %s socket called without space",
		   buf->is_from_socket ? "from" : "to") ;
	sl_suspend_fd_handler(buf->in_fd, SL_INPUT) ;
    }
}


/** Install the I/O handler functions for the socket and initialize
 * the I/O buffers.
 * 
 * @param sockfd   socket file descriptor
 * @param in_fd    input file descriptor to read from
 * @param out_fd   output file descriptor to write to
 */
void install_dgram_handlers(int sockfd, int in_fd, int out_fd)
{
    /* This function has to install four I/O handlers:
     *  - read from socket
     *  - write to stdout
     *  - read from stdin
     *  - write to socket.
     */

    DEBUG_FUNCTION(("install_dgram_handlers(%d)", sockfd)) ;
    fromsock_buf.is_from_socket = 1 ;
    fromsock_buf.in_fd = sockfd ;
    fromsock_buf.out_fd = out_fd ;

    /* TODO: perhaps prepare translation buffers for insert_crs() */

    if (sl_install_fd_handler(sockfd, SL_INPUT, read_handler,
                              &fromsock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing socket read handler") ;
    }
    if (sl_install_fd_handler(fromsock_buf.out_fd, SL_OUTPUT, write_handler,
                              &fromsock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing socket write handler") ;
    }
    sl_suspend_fd_handler(STDOUT_FILENO, SL_OUTPUT) ;

    tosock_buf.is_from_socket = 0 ;
    tosock_buf.in_fd = in_fd ;
    tosock_buf.out_fd = sockfd ;

    /* TODO: perhaps prepare translation buffers for insert_crs() */
    if (options.translate_lf) {
	tr_data = ck_malloc(2 * options.buffersize, "dgram translation data") ;
    }

    if (sl_install_fd_handler(tosock_buf.in_fd, SL_INPUT, read_handler,
                              &tosock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing stdin read handler") ;
    }
    if (sl_install_fd_handler(sockfd, SL_OUTPUT, write_handler,
                              &tosock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing stdout write handler") ;
    }
    sl_suspend_fd_handler(sockfd, SL_OUTPUT);
}



/* EOF */
