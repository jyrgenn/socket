/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: streamio.c 8 2005-05-18 14:40:31Z ni $
 *
 * stream I/O handlers for Socket version 2.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "bsd_sysexits.h"
#include "socket2.h"
#include "selectloop.h"

char streamio_ident[] = "$Id: streamio.c 8 2005-05-18 14:40:31Z ni $" ;


typedef struct STREAMBUFFER {
    int is_from_socket ;	/* Non-zero if in_fd is the socket;
				 * otherwise it's out_fd. */
    char *data ;                /* Data as read. */
    char *tr_data ;             /* Data after CR/LF-LF transformation
                                 * (if applicable, otherwise same as data). */
    int size ;                  /* Allocated size of buffer. */
    int used ;                  /* Amount of data in buffer. */
    int written ;               /* Non-zero if not all data could be
                                 * written; indicates place to start
                                 * next write with. */
    int in_fd ;                 /* File descriptor to read from. */
    LONGLONG total_read ;       /* Amount of data read from in_fd.
				 * Be aware that the LONGLONG will,
				 * at 1 GB/s, overflow after 272
				 * years. */
    int out_fd ;                /* File descriptor to write to. */
    LONGLONG total_written ;    /* Amount of data written to out_fd. */
} streambuffer_t ;

static streambuffer_t fromsock_buf, tosock_buf ;


static void read_handler(int fd, void *context)
{
    streambuffer_t *buf = context ;	/* IO buffer to use. */
    int nbytes ;		/* Number of bytes read. */

    DEBUG_FUNCTION(("read_handler(%d, %p)", fd, context)) ;
    reset_receive_timeout() ;
    nbytes = read(buf->in_fd, buf->data, buf->size) ;
    DEBUG(S2_DBG_IOCALLS, s2dbg_print_iocall("read", buf->in_fd, nbytes)) ;
    switch (nbytes) {
      case -1:			
	/* Read error. */
        syserr_exit(EX_IOERR, "error reading from %s",
                    buf->is_from_socket ? "socket" : "standard input") ;
        break ;                 /* For cosmetic raisins. */
      case 0:
	/* EOF on read. */
	if (buf->is_from_socket) {
	    verbose(VERBOSE_CONNECT, "connection closed by peer, ") ;
	    if (!options.survive_halfclose) {
		verbose(VERBOSE_CONNECT, "exiting\n") ;
		exit(0) ;
	    }
	    verbose(VERBOSE_CONNECT, "continuing\n") ;
	} else {	    
	    verbose(VERBOSE_NET, "EOF on standard input\n") ;
	    if (options.quit_on_eof) {
		exit(0) ;
	    } else {
		/* Otherwise do half close and deinstall read handler. */
		if (sl_remove_fd_handler(buf->in_fd, SL_INPUT) != 0) {
		    syserr_exit(EX_SOFTWARE, "error unhandling %s fd %d",
				buf->is_from_socket ? "socket" : "input",
				buf->in_fd) ;
		}
		if (shutdown(buf->out_fd, SHUT_WR) != 0) {
		    syserr_printf("half-close on socket failed") ;
		}
	    }
            close(buf->in_fd) ;
        }
        break ;
      default:
	/* Successful read. */
        buf->total_read += nbytes ;
	buf->used += nbytes ;
	if (buf->is_from_socket) {
	    verbose(VERBOSE_MSGS, "-> read %d bytes from socket, %Ld total\n",
		    nbytes, buf->total_read) ;
	}
	if (options.text_dump) {
	    text_dump_packet(buf->data, nbytes, buf->is_from_socket) ;
	}
	if (options.hex_dump) {
	    hexdump_packet(buf->data, nbytes, buf->is_from_socket) ;
	}
	if (options.translate_lf) {
	    if (buf->is_from_socket) {
		buf->used = strip_crs(buf->data, buf->used, buf->tr_data) ;
	    } else {
		buf->used = insert_crs(buf->data, buf->used, buf->tr_data) ;
	    }
	}
	sl_suspend_fd_handler(buf->in_fd, SL_INPUT) ;
        sl_resume_fd_handler(buf->out_fd, SL_OUTPUT) ;
    }
}


static void write_handler(int fd, void *context)
{
    streambuffer_t *buf = context ;
    int nbytes ;

    DEBUG_FUNCTION(("write_handler(%d, %p)", fd, context)) ;
    reset_receive_timeout() ;
    nbytes = write(buf->out_fd, buf->tr_data + buf->written,
                   buf->used - buf->written) ;
    DEBUG(S2_DBG_IOCALLS, s2dbg_print_iocall("write", buf->out_fd, nbytes)) ;
    if (nbytes == -1) {
        syserr_exit(EX_IOERR, "error writing to %s",
                    buf->is_from_socket ? "standard output" : "socket") ;
    }
    buf->total_written += nbytes ;
    buf->written += nbytes ;
    if (buf->written >= buf->used) {
        buf->written = 0 ;
        buf->used = 0 ;
        sl_suspend_fd_handler(buf->out_fd, SL_OUTPUT) ;
        sl_resume_fd_handler(buf->in_fd, SL_INPUT) ;
    }   
    if (!buf->is_from_socket) {
	verbose(VERBOSE_MSGS, "<- wrote %d bytes to socket, %Ld total\n",
		nbytes, buf->total_written) ;
    }
}


/** Install the I/O handler functions for the socket and initialize
 * the I/O buffers.
 * 
 * @param sockfd socket file descriptor
 */
void install_stream_handlers(int sockfd, int in_fd, int out_fd)
{
    /* This function has to install four I/O handlers:
     *  - read from socket
     *  - write to stdout
     *  - read from stdin
     *  - write to socket.
     */

    DEBUG_FUNCTION(("install_stream_handlers(%d)", sockfd)) ;
    fromsock_buf.is_from_socket = 1 ;
    fromsock_buf.data = ck_malloc(options.buffersize, "in buffer") ;
    fromsock_buf.size = options.buffersize ;
    fromsock_buf.in_fd = sockfd ;
    fromsock_buf.out_fd = out_fd ;

    if (options.translate_lf) {
        fromsock_buf.tr_data = ck_malloc(options.buffersize, "in buffer tr") ;
    } else {
        fromsock_buf.tr_data = fromsock_buf.data ;
    }

    if (sl_install_fd_handler(sockfd, SL_INPUT, read_handler,
                              &fromsock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing socket read handler") ;
    }
    if (sl_install_fd_handler(fromsock_buf.out_fd, SL_OUTPUT, write_handler,
                              &fromsock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing socket write handler") ;
    }

    tosock_buf.is_from_socket = 0 ;
    tosock_buf.data = ck_malloc(options.buffersize, "out buffer") ;
    tosock_buf.size = options.buffersize ;
    tosock_buf.in_fd = in_fd ;
    tosock_buf.out_fd = sockfd ;

    if (options.translate_lf) {
        tosock_buf.tr_data = ck_malloc(2 * options.buffersize,
				       "out buffer tr") ;
    } else {
        tosock_buf.tr_data = tosock_buf.data ;
    }

    if (sl_install_fd_handler(tosock_buf.in_fd, SL_INPUT, read_handler,
                              &tosock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing stdin read handler") ;
    }
    if (sl_install_fd_handler(sockfd, SL_OUTPUT, write_handler,
                              &tosock_buf, 0) == -1) {
        syserr_exit(EX_SOFTWARE, "error installing stdout write handler") ;
    }
}


/* EOF */
