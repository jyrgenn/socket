/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: selectloop.h 8 2005-05-18 14:40:31Z ni $
 *
 * Select loop header file for Socket version 2. Although this
 * module is distributed with the Socket program, it is intended to
 * be generally usable.
 *
 */

#ifdef MAIN_C
char selectloop_h_ident[] = "$Id: selectloop.h 8 2005-05-18 14:40:31Z ni $" ;
#endif /* MAIN_C */

#ifndef SELECTLOOP_H
#define SELECTLOOP_H

/* The select loop calls previously installed handler functions on
 * certain events -- a file descriptor becoming readable, writable or
 * signalling an exception, or a timer expiring. Timers can be defined
 * for absolute or relative time and are optionally repeating.
 *
 * Handler functions can be installed and removed, suspended and
 * resumed. A suspended handler function will not be called on its
 * event until it is resumed. A removed handler function is no
 * longer known at all.
 */

#include <limits.h>
#include <unistd.h>

/***********************************************************************
 * Some definitions.
 */

/* I assume that limits.h defines an OPEN_MAX as the maximum number of
 * file descriptors a process can open. If not, define an OPEN_MAX as
 * appropriate for your system. */
#define SL_MAXFDS OPEN_MAX      /* Maximum number of file descriptors
                                 * in the select loop. */

/* Data type for a 64-bit integer or another appropriate sufficently
 * large data type. Define this to something else if you don't use
 * GCC or a GCC-compatible compiler. The 64-bit value will carry us
 * well beyond the year 200000 with a microsecond resolution; I
 * think that's enough.*/
#ifndef LONGLONG
#define LONGLONG long long
#endif /* LONGLONG */

/* Definitions for the reading, writing, and exception conditions on a
 * file descriptor, corresponding to the respective file descriptor
 * set arguments to select(). */
#define SL_INPUT  0
#define SL_OUTPUT 1
#define SL_EXCEPT 2

/* Debug flags. Define SL_DEBUG in the Makefile to enable debugging.
 */
#define SL_DBG_FDSETS	0x00000001 /* Print FD sets before select(). */
#define SL_DBG_TIMERS	0x00000002 /* Print next timer value before select(). */


/***********************************************************************
 * Functions. For file descriptors and timers there are functions to
 * install, remove, suspend, and resume a handler function. File
 * descriptor handlers can referred to by the file descriptor and
 * the condition (SL_INPUT, SL_OUTPUT, SL_EXCEPT). For timers the
 * "install" functions returns a pointer by which the handler can be
 * referred to. Each handler function is passed a context pointer.
 * The `free_function' is called on the context when the handler is
 * removed (if the free_function is non-null). All functions return
 * 0 on success and -1 on error. In the latter case, errno is set to
 * an appropriate value.
 */

/** Type of a file descriptor handler function.
 *
 * @param fd       The file descriptor.
 * @param context  The context pointer supplied when installing the handler.
 */
typedef void (*sl_fd_handler_t)(int fd, void *context) ;


/** Timer handle to suspend, resume, or remove a timer handler. */
typedef struct SL_TIMER sltimer_t ;

/** Type of a timer handler function.
 *
 * @param context  The context pointer supplied when installing the handler.
 */
typedef void (*sl_timer_handler_t)(sltimer_t *timer, void *context) ;


/** Install a file descriptor handler. Call `handler' with `fd' and
 * `context' when `condition' occurs on `fd'. Call `free_function'
 * with `context' when the handler is removed.
 *
 * @param fd             File descriptor to select on.
 * @param condition      One of SL_INPUT, SL_OUTPUT, SL_EXCEPT.
 * @param handler        Function to call on `condition' at `fd'.
 * @param context        The context pointer passed to the handler function.
 * @param free_function  Function to call with context on handler remove.
 * @return               Zero on success, -1 on error.
 */
int sl_install_fd_handler(int fd, int condition, sl_fd_handler_t handler,
                          void *context,
                          void (*free_function)(void *context)) ;

/** Remove a file descriptor handler. Call previously defined
 * `free_function' with `context' (see sl_install_fd_handler()). After
 * deletion, a file descriptor handler will no longer be called, and
 * it will no longer be accessible for suspension, resume, or deletion.
 *
 * @param fd         File descriptor to select on.
 * @param condition  One of SL_INPUT, SL_OUTPUT, SL_EXCEPT.
 * @return           Zero on success, -1 on error.
 */
int sl_remove_fd_handler(int fd, int condition) ;

/** Suspend a file descriptor handler. While it is suspended, a file
 * descriptor handle will not be called. It can be resumed with
 * sl_resume_fd_handler(). Suspending an already suspended file
 * descriptor handler is not an error.
 *
 * @param fd         File descriptor to select on.
 * @param condition  One of SL_INPUT, SL_OUTPUT, SL_EXCEPT.
 * @return           Zero on success, -1 on error.
 */
int sl_suspend_fd_handler(int fd, int condition) ;

/** Resume a file descriptor handler. If it was suspended, a file
 * descriptor handle will be called again after resume. Resuming a
 * non-suspended file descriptor handler is not an error.
 *
 * @param fd         File descriptor to select on.
 * @param condition  One of SL_INPUT, SL_OUTPUT, SL_EXCEPT.
 * @return           Zero on success, -1 on error.
 */
int sl_resume_fd_handler(int fd, int condition) ;


/** Install a timer. Call `handler' with `context' at time `when' (in
 * microseconds). If `absolute' is non-zero, `when' is an absolute
 * time (counting from 1970-01-01 00:00:00), otherwise it is relative
 * from "now". If `repeat' is non-zero, repeat the call after `repeat'
 * microseconds until the timer is removed. If `when' is zero,
 * `absolute' is nonzero, and `repeat' is non-zero, the timer handler
 * is called after `repeat' microseconds. When the timer is removed,
 * call `free_function' with `context'.
 *
 * @param when           Time to call `handler' (in microseconds).
 * @param absolute       If non-zero, `when' is absolute, else relative.
 * @param repeat         If non-zero, repeat the call after every `repeat'
 *                       microseconds.
 * @param handler        Function to call.
 * @param context        The context pointer passed to the handler function.
 * @param free_function  Function to call with context on handler remove.
 * @return               An sl_timer handle.
 */
sltimer_t *sl_install_timer(LONGLONG when, int absolute, LONGLONG repeat,
                            sl_timer_handler_t handler,
                            void *context,
                            void (*free_function)(void *context)) ;

/** Remove a timer. Call previously defined `free_function' with
 * `context' (see sl_install_timer()). After deletion, a timer will no
 * longer be called, and it will no longer be accessible for
 * suspension, resume, or deletion.
 *
 * @param timer  Handle of a previously installed timer.
 * @return       Zero on success, -1 on error.
 */
int sl_remove_timer(sltimer_t *timer) ;

/** Suspend a timer. While it is suspended, a timer handler will not
 * be called. It can be resumed with sl_resume_timer(). Suspending an
 * already suspended timer is not an error.
 *
 * @param timer  Handle of a previously installed timer.
 * @return       Zero on success, -1 on error.
 */
int sl_suspend_timer(sltimer_t *timer) ;

/** Resume a timer. While it is resumed, a timer handler will not be
 * called. It can be resumed with sl_resume_timer(). Resuming an
 * already resumed timer is not an error. If the timer was
 * non-repeating and has expired before resuming, it will not be
 * called.
 *
 * @param timer  Handle of a previously installed timer.
 * @return       Zero on success, -1 on error.
 */
int sl_resume_timer(sltimer_t *timer) ;

/** Reset a timer. This is only possible for a relative timer. The
 * timer will be handled as if it has been newly installed.
 *
 * @param timer  Handle of a previously installed timer.
 * @return       Zero on success, -1 on error.
 */
int sl_reset_timer(sltimer_t *timer) ;

/** Start the select loop. The loop will be run until it is explicitly
 * ended (by calling sl_terminate_loop()) or a select error other than
 * EINTR occurs. In the latter case sl_run_loop() returns -1 with
 * errno set, otherwise the value passed to sl_terminate_loop().
 *
 * @return    -1 on select error or the argument of sl_terminate_loop().
 */
int sl_run_loop(void) ;

/** Terminate the select loop. If the argument is negative, the select
 * loop will be terminated immediately; otherwise all outstanding
 * events are handled before the loop is terminated. sl_run_loop()
 * will return the argument of sl_terminate_loop().
 *
 * @param argument  Return value of sl_run_loop().
 */
void sl_terminate_loop(int argument) ;

#endif /* SELECTLOOP_H */
/* EOF */
