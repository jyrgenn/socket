/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: client.c 17 2007-07-11 10:00:16Z ni $
 *
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/errno.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/un.h>

#include "bsd_sysexits.h"
#include "socket2.h"

char client_ident[] = "$Id: client.c 17 2007-07-11 10:00:16Z ni $" ;


/** Create a client socket of the specified type and connect it to the
 * specified address.
 * 
 * @param socktype   The `type' argument to socket().
 * @param sap        Pointer to socket address struct.
 * @param sa_size    Size of the socket address.
 * @return           The socket file descriptor or -1 on failure.
 */
int connect_client_socket(int socktype, struct sockaddr *sap, int sa_size)
{
    int sockfd ;                /* Socket file descriptor. */
    
    DEBUG_FUNCTION(("connect_client_socket(%d, %p, %d)",
                    socktype, sap, sa_size)) ;
    if ((sockfd = socket(sap->sa_family, socktype, 0)) < 0) {
        return -2 ;
    }

    verbose(VERBOSE_CONNECT, "connecting to %s...", sockaddr_string(sap)) ;
    if (connect(sockfd, sap, sa_size) == -1) {
        verbose(VERBOSE_CONNECT, "\n") ;
        close(sockfd) ;
        return -1 ;
    }

    verbose(VERBOSE_CONNECT, "done\n") ;
    return sockfd ;
}


/** Do client functions common to all (supported) socket types.
 * 
 * @param socktype   The `type' argument to socket().
 * @param sap        Pointer to socket address struct.
 * @param sa_size    Size of the socket address.
 */
static void client_common(int socktype, struct sockaddr *sap, int sa_size)
{
    int sockfd ;                /* Socket file descriptor. */
    int in_fd = STDIN_FILENO ;
    int out_fd = STDOUT_FILENO ;

    DEBUG_FUNCTION(("client_common(%d, %p, %d)", socktype, sap, sa_size)) ;
    /* The function connect_client_socket() could be integrated here
     * completely, but I want to make it easily reusable for other
     * programs. */
    sockfd = connect_client_socket(socktype, sap, sa_size) ;
    DEBUG(S2_DBG_FDVALS, fprintf(s2_debug_stream, "sockfd = %d", sockfd)) ;
    if (sockfd < 0) {
        switch (sockfd) {
          case -2:
            syserr_exit(EX_OSERR, "cannot create socket") ;
            break ;
          case -1:
            syserr_exit(EX_TEMPFAIL, "cannot connect socket") ;
            break ;
          default:
            syserr_exit(EX_UNAVAILABLE, "client socket failed") ;
            break ;
        }
    }
    if (options.run_program) {
        connect_child_proc(options.run_program, sockfd, &out_fd, &in_fd) ;
    }
    switch (socktype) {
      case SOCK_STREAM:
      case SOCK_SEQPACKET:
        install_stream_handlers(sockfd, in_fd, out_fd) ;
        break ;
      case SOCK_DGRAM:
        install_dgram_handlers(sockfd, in_fd, out_fd) ;
        break ;
      default:
        err_exit(EX_SOFTWARE, "invalid socket type at %s:%d",
                 __FILE__, __LINE__) ;
        break ;
    }
}


/** Create a client for a UNIX domain socket.
 *
 * @param pathname   path name of UNIX domain socket
 */
void client_unix(char *pathname)
{
    struct sockaddr_un *sunp ;
    int sun_len = strlen(pathname) + 1 +
            offsetof(struct sockaddr_un, sun_path) ;
    int socktype = options.sock_seqpacket ? SOCK_SEQPACKET : SOCK_STREAM;

    DEBUG_FUNCTION(("client_unix(%s)", pathname)) ;
    sunp = ck_malloc(sun_len, "sockaddr_un") ;
#ifndef HAS_NO_SUN_LEN
    sunp->sun_len = sun_len ;
#endif  /* HAS_NO_SUN_LEN */
    sunp->sun_family = AF_UNIX ;
    strcpy(sunp->sun_path, pathname) ;
    client_common(socktype, (struct sockaddr *) sunp, sun_len) ;
    /* Now fall back to main, where the select loop is called. */
}


/** Create a client for a TCP or UDP socket.
 *
 * @param host_port    name or ip address of host and service name or
 *                     port number to connect to (host:port)
 */
void client_ip(char *host_port)
{
    static struct sockaddr_in s_in ;
    char *service ;
    int socktype = options.socktype_udp ? SOCK_DGRAM
            : (options.sock_seqpacket ? SOCK_SEQPACKET : SOCK_STREAM);
    

    DEBUG_FUNCTION(("client_ip(%s)", host_port)) ;
    switch (resolve_host_port(host_port, &s_in, &service,
                              options.socktype_udp ? "udp" : "tcp")) {
      case RESERR_INVALID:
        err_exit(EX_DATAERR, "invalid host:port argument \"%s\"",
                 host_port) ;
        break ;
      case RESERR_PORT:
        err_exit(EX_DATAERR, "could not resolve service \"%s\"",
                 service) ;
        break ;
      case RESERR_HOST:
        err_exit(EX_NOHOST, "host \"%s\" not found (%s)",
                 host_port, hstrerror(h_errno)) ;
        break ;
    }

    client_common(socktype, (struct sockaddr *) &s_in, sizeof(s_in)) ;
    /* Now fall back to main, where the select loop is called. */
}

/*
 * Local Variables:
 * indent-tabs-mode: nil
 * End:
 */
/* EOF */
