/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: selectloop.c 8 2005-05-18 14:40:31Z ni $
 *
 * Select loop for Socket version 2.
 *
 */

#include "selectloop.h"

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#ifndef OPEN_MAX
#define OPEN_MAX 256
#endif /* OPEN_MAX */

#ifdef SL_DEBUG
#include <stdio.h>
#include "socket2.h"

#else  /* SL_DEBUG */

#define SLDEBUG_FUNCTION(func)

#endif /* SL_DEBUG */


/* Marker for a valid timer; intended to protect against invalid
 * timer entry pointers supplied by the application.
 */
#define TIMER_IN_USE 0x15600D	/* "ISGOOD" */

char selectloop_ident[] = "$Id: selectloop.c 8 2005-05-18 14:40:31Z ni $" ;

/***********************************************************************
 * Data definitions
 */

typedef struct FD_HANDLER {
    int in_use ;
    int active ;
    sl_fd_handler_t handler ;
    void *context ;
    void (*free_function)(void *context) ;
} fdh_t ;

/* Array of all possible file descriptor handlers, indexed by file
 * descriptor and condition. */
static fdh_t fd_handler[OPEN_MAX][3] ;

/* Corresponding file descriptor sets. */
static fd_set fdset[3] ;

/* Highest file descriptor ever used. */
static int max_fd = 0 ;


struct SL_TIMER {               /* Is typedef'ed as sltimer_t in the header. */
    int in_use ;                /* Non-zero if in use at all. */
    int active ;
    LONGLONG when ;             /* When timer is due. */
    LONGLONG relative ;         /* Original value if relative;
                                 * used for reset. */
    LONGLONG repeat ;           /* Repetition interval (iff). */
    sl_timer_handler_t handler; /* Handler function. */
    void *context ;             /* Context for handler function. */
    void (*free_function)(void *context) ;
    struct SL_TIMER *next;      /* Next in list. */
    struct SL_TIMER **previousp ; /* Pointer to the pointer to this. */
} ;

/* List of all timers. */
static sltimer_t *timerlist ;


/* All functions changing the timers set this to non-zero to signal
 * a timer change to the select loop. In this case the select loop
 * is restarted to work with the updated timer. */
static int timer_changed = 0 ;

/* sl_terminate_loop() sets this to non-zero to signal a stop
 * condition to the select loop. */
static int loop_terminated = 0 ;

/* sl_terminate_loop() sets this value; sl_run_loop() will then return
 * this value. */
static int terminate_argument = 0 ;

/* Timer just being called. */
static sltimer_t *current_timer = 0 ;


/***********************************************************************
 * Internal functions
 */

/** Insert timer into timer list.
 * @param tp pointer to timer handler struct
 */
static void insert_timer(sltimer_t *tp)
{
    sltimer_t *tl ;             /* Timer in list. */

    SLDEBUG_FUNCTION(("insert_timer(%p)", tp)) ;
    if (timerlist == 0) {
        timerlist = tp ;
        tp->next = 0 ;
        tp->previousp = &timerlist ;
    } else {
        tl = timerlist ;
        do {
            if (tl->when > tp->when) {
                *tl->previousp = tp ;
                tp->previousp = tl->previousp ;
                tl->previousp = &tp->next ;
                tp->next = tl ;
                break ;
            } else if (tl->next) {
                tl = tl->next ;
            } else {
                tp->next = 0 ;
                tl->next = tp ;
                tp->previousp = &tl->next ;
                break ;
            }
        } while (tl) ;
    }
}


/** Return the current time in microseconds.
 */
static LONGLONG timer_now(void)
{
    struct timeval tv ;
    
    gettimeofday(&tv, 0) ;
    return (LONGLONG) tv.tv_sec * 1000000 + tv.tv_usec ;
}

#define CHECK_FD_ARGS(fd, cond)                    \
do {                                               \
    if (cond<0 || cond>2 || fd<0 || fd>OPEN_MAX) { \
        errno = EINVAL ;                           \
        return -1 ;                                \
    }                                              \
} while (0)

#define CHECK_FD_FREE(fdh)                         \
do {                                               \
    if (fdh->in_use) {                             \
        errno = EEXIST ;                           \
        return -1 ;                                \
    }                                              \
} while (0)

#define CHECK_FD_USED(fdh)                         \
do {                                               \
    if (!fdh->in_use) {                            \
        errno = EINVAL ;                           \
        return -1 ;                                \
    }                                              \
} while (0)

#define CHECK_TIMER_OK(tp)                         \
do {                                               \
    if (!(tp && tp->in_use == TIMER_IN_USE)) {     \
        errno = EFAULT ;                           \
        return -1 ;                                \
    }                                              \
} while (0)



/***********************************************************************
 * Debug functions
 */
#ifdef SL_DEBUG

static unsigned long sl_debug_flags = SL_DEBUG ;
				/* Bitset of debugging flags. */
static FILE *sl_debug_stream = 0 ;
				/* Stdio stream to write debugging
				 * output to. */

#define SLDEBUG(flag, code)        \
    if (sl_debug_flags & flag) { \
	code ;                   \
    }

static void sldbg_print_fdsets(char *prefix, fd_set set[3])
{
    int i ;			/* I/O direction. */
    char fdbit[OPEN_MAX + 2] ;	/* Binary representation of fdset bit. */

    SLDEBUG_FUNCTION(("sldbg_print_fdsets(%s, set[3])", prefix)) ;
    for (i = 0; i < 3; i++) {
	int bit ;		/* Fdset bit position. */

	fprintf(sl_debug_stream, "FDSET %s %d: ", prefix, i) ;
	for (bit = 0; bit <= max_fd; bit++) {
	    fdbit[bit] = FD_ISSET(bit, &set[i]) ? '1' : '0' ;
	}
	fdbit[bit++] = '\n' ;
	fdbit[bit++] = '\0' ;
	fputs(fdbit, sl_debug_stream) ;
    }
}

static void sldbg_print_seltvp(char *prefix, struct timeval *tvptr)
{
    SLDEBUG_FUNCTION(("sldbg_print_seltvp(%s, %p)", prefix, tvptr)) ;
    fprintf(sl_debug_stream, "%s ", prefix) ;
    if (tvptr) {
        fprintf(sl_debug_stream, "%lu:%lu\n", tvptr->tv_sec, tvptr->tv_usec) ;
    } else {
        fprintf(sl_debug_stream, "0\n") ;
    }
}


static void sldbg_print_timerlist(char *prefix, sltimer_t *tlist)
{
    SLDEBUG_FUNCTION(("sldbg_print_timerlist(%s, %p)", prefix, tlist)) ;
    while (tlist) {
        fprintf(sl_debug_stream, "  %s %qd\n", prefix,
                tlist->when - timer_now()) ;
        tlist = tlist->next ;
    }
}

static void sldbg_initialize(void)
{
    SLDEBUG_FUNCTION(("sldbg_initialize()")) ;
    sl_debug_stream = stderr ;
}
    
#else  /* SL_DEBUG */
#define SLDEBUG(a, b)
#endif /* SL_DEBUG */



/***********************************************************************
 * Public functions: Handler handling
 */

/** Install a file descriptor handler. Call `handler' with `fd' and
 * `context' when `condition' occurs on `fd'. Call `free_function'
 * with `context' when the handler is removed.
 * @param fd            file descriptor to select on
 * @param condition     one of SL_INPUT, SL_OUTPUT, SL_EXCEPT
 * @param handler       function to call on `condition' at `fd'
 * @param context       the context pointer passed to the handler function
 * @param free_function function to call with context on handler remove
 * @return              zero on success, -1 on error
 */
int sl_install_fd_handler(int fd, int condition, sl_fd_handler_t handler,
                          void *context, void (*free_function)(void *context))
{
    fdh_t *fdh ;

    SLDEBUG_FUNCTION(("sl_install_fd_handler(%d, %d, %p, %p, %p)",
		    fd, condition, handler, context, free_function)) ;
    CHECK_FD_ARGS(fd, condition) ;
    fdh = &fd_handler[fd][condition] ;
    CHECK_FD_FREE(fdh) ;

    fdh->in_use = 1 ;
    fdh->active = 1 ;
    fdh->handler = handler ;
    fdh->context = context ;
    fdh->free_function = free_function ;
    FD_SET(fd, &fdset[condition]) ;
    if (fd > max_fd) {
        max_fd = fd ;
    }

    return 0 ;
}

/** Remove a file descriptor handler. Call previously defined
 * `free_function' with `context' (see sl_install_fd_handler()). After
 * deletion, a file descriptor handler will no longer be called, and
 * it will no longer be accessible for suspension, resume, or deletion.
 *
 * @param fd            file descriptor to select on
 * @param condition     one of SL_INPUT, SL_OUTPUT, SL_EXCEPT
 * @return              zero on success, -1 on error
 */
int sl_remove_fd_handler(int fd, int condition)
{
    fdh_t *fdh ;

    SLDEBUG_FUNCTION(("sl_remove_fd_handler(, %d, %d)", fd, condition)) ;
    CHECK_FD_ARGS(fd, condition) ;
    fdh = &fd_handler[fd][condition] ;
    CHECK_FD_USED(fdh) ;

    FD_CLR(fd, &fdset[condition]) ;
    fdh->in_use = 0 ;
    if (fdh->context && fdh->free_function) {
	fdh->free_function(fdh->context) ;
    }

    return 0 ;
}

/** Suspend a file descriptor handler. While it is suspended, a file
 * descriptor handler will not be called. It can be resumed with
 * sl_resume_fd_handler(). Suspending an already suspended file
 * descriptor handler is not an error.
 * @param fd            file descriptor to select on
 * @param condition     one of SL_INPUT, SL_OUTPUT, SL_EXCEPT
 * @return              zero on success, -1 on error
 */
int sl_suspend_fd_handler(int fd, int condition)
{
    fdh_t *fdh ;

    SLDEBUG_FUNCTION(("sl_suspend_fd_handler(%d, %d)", fd, condition)) ;
    CHECK_FD_ARGS(fd, condition) ;
    fdh = &fd_handler[fd][condition] ;
    CHECK_FD_USED(fdh) ;

    FD_CLR(fd, &fdset[condition]) ;
    fdh->active = 0 ;

    return 0 ;
}

/** Resume a file descriptor handler. If it was suspended, a file
 * descriptor handle will be called again after resume. Resuming a
 * non-suspended file descriptor handler is not an error.
 * @param fd            file descriptor to select on
 * @param condition     one of SL_INPUT, SL_OUTPUT, SL_EXCEPT
 * @return              zero on success, -1 on error
 */
int sl_resume_fd_handler(int fd, int condition)
{
    fdh_t *fdh ;

    SLDEBUG_FUNCTION(("sl_resume_fd_handler(%d, %d)", fd, condition)) ;
    CHECK_FD_ARGS(fd, condition) ;
    fdh = &fd_handler[fd][condition] ;
    CHECK_FD_USED(fdh) ;

    FD_SET(fd, &fdset[condition]) ;
    fdh->active = 1 ;

    return 0 ;
}


/** Install a timer. Call `handler' with `context' at time `when' (in
 * microseconds). If `absolute' is non-zero, `when' is an absolute
 * time (counting from 1970-01-01 00:00:00), otherwise it is relative
 * from "now". If `repeat' is non-zero, repeat the call after `repeat'
 * milliseconds until the timer is removed. If `when' is zero,
 * `absolute' is nonzero, and `repeat' is non-zero, the timer handler
 * is called after `repeat' microseconds. When the timer is removed,
 * call `free_function' with `context'.
 * @param when          time to call `handler' (in microseconds)
 * @param absolute      if non-zero, `when' is absolute, else relative
 * @param repeat        if non-zero, repeat the call after every `repeat'
 *                      microseconds
 * @param handler       function to call
 * @param context       the context pointer passed to the handler function
 * @param free_function function to call with context on handler remove
 * @return              an sl_timer handle
 */
sltimer_t *sl_install_timer(LONGLONG when, int absolute, LONGLONG repeat,
                            sl_timer_handler_t handler,
                            void *context, void (*free_function)(void *context))
{
    sltimer_t *newtimer ;

    SLDEBUG_FUNCTION(("sl_install_timer(%qd, %d, %qd, %p, %p, %p)",
		    when, absolute, repeat, handler, context, free_function)) ;
    newtimer = malloc(sizeof(sltimer_t)) ;
    if (newtimer == 0) {
        errno = ENOMEM ;
        return 0 ;
    }

    newtimer->in_use = TIMER_IN_USE ;
    newtimer->active = 1 ;
    if (absolute) {
        newtimer->when = when ;
        newtimer->relative = 0 ;
    } else {
        newtimer->when = when + timer_now() ;
        newtimer->relative = when ;
    }
    newtimer->repeat = repeat ;
    newtimer->handler = handler ;
    newtimer->context = context ;
    newtimer->free_function = free_function ;
    newtimer->next = 0 ;
    newtimer->previousp = 0 ;
    insert_timer(newtimer) ;
    
    timer_changed = 1 ;

    return (sltimer_t *) newtimer ;
}

static void free_timer(sltimer_t *timer)
{
    SLDEBUG_FUNCTION(("free_timer(%p)", timer)) ;
    if (timer->context && timer->free_function) {
	timer->free_function(timer->context) ;
    }
    free(timer) ;
}

/** Remove a timer. Call previously defined `free_function' with
 * `context' (see sl_install_timer()). After deletion, a timer will no
 * longer be called, and it will no longer be accessible for
 * suspension, resume, or deletion.
 * @param timer handle of a previously installed timer
 * @return              zero on success, -1 on error
 */
int sl_remove_timer(sltimer_t *timer)
{
    SLDEBUG_FUNCTION(("sl_remove_timer(%p)", timer)) ;
    CHECK_TIMER_OK(timer) ;
    timer->in_use = 0 ;
    if (timer != current_timer) {
        if (timer->next) {
            timer->next->previousp = timer->previousp ;
        }
        *timer->previousp = timer->next ;
        free_timer(timer) ;
	timer_changed = 1 ;
    }
    /* If this is the current_timer, it is freed in the loop anyway
     * if no longer marked as in use. */
    return 0 ;
}

/** Suspend a timer. While it is suspended, a timer handler will not
 * be called. It can be resumed with sl_resume_timer(). Suspending an
 * already suspended timer is not an error.
 * @param timer handle of a previously installed timer
 * @return      zero on success, -1 on error
 */
int sl_suspend_timer(sltimer_t *timer)
{
    SLDEBUG_FUNCTION(("sl_suspend_timer(%p)", timer)) ;
    CHECK_TIMER_OK(timer) ;
    timer->active = 0 ;
    timer_changed = 1 ;
    return 0 ;
}

/** Resume a timer. While it is resumed, a timer handler will not
 * be called. It can be resumed with sl_resume_timer(). Resuming an
 * already resumed timer is not an error.
 * @param timer handle of a previously installed timer
 * @return      zero on success, -1 on error
 */
int sl_resume_timer(sltimer_t *timer)
{
    SLDEBUG_FUNCTION(("sl_resume_timer(%p)", timer)) ;
    CHECK_TIMER_OK(timer) ;
    timer->active = 1 ;
    timer_changed = 1 ;
    return 0 ;
}

/** Reset a timer. This is only possible for a relative timer. The
 * timer will be handled as if it has been newly installed.
 * @param timer handle of a previously installed timer
 * @return      zero on success, -1 on error
 */
int sl_reset_timer(sltimer_t *timer)
{
    SLDEBUG_FUNCTION(("sl_reset_timer(%p)", timer)) ;
    CHECK_TIMER_OK(timer) ;
    if (timer->relative) {
        timer->when = timer->relative + timer_now() ;
        timer_changed = 1 ;
        return 0 ;
    } else {
	errno = ENODEV ;
        return -1 ;
    }
}


/***********************************************************************
 * Public functions: Run and terminate loop
 */

/** Start the select loop. The loop will be run until it is explicitly
 * ended (by calling sl_terminate_loop()) or a select error other than
 * EINTR occurs. In the latter case sl_run_loop() returns -1 with
 * errno set, otherwise the value passed to sl_terminate_loop().
 * @return -1 on select error or the argument of sl_terminate_loop()
 */
int sl_run_loop(void)
{
    fd_set sel_set[3] ;
    struct timeval tv ;
    struct timeval *tvptr ;
    int selret ;

    SLDEBUG_FUNCTION(("sl_run_loop()")) ;
#ifdef SL_DEBUG
    sldbg_initialize() ;
#endif
    
    do {
        /* Copy file descriptor sets (select() changes them). */
        sel_set[0] = fdset[0] ;
        sel_set[1] = fdset[1] ;
        sel_set[2] = fdset[2] ;

        /* Set up timer. */
        if (timerlist) {
            LONGLONG relative = timerlist->when - timer_now() ;

            if (relative < 0) {
                relative = 0 ;
            }
            tv.tv_sec = relative / 1000000 ;
            tv.tv_usec = relative % 1000000 ;
            tvptr = &tv ;
        } else {
            tvptr = 0 ;
        }
	

	SLDEBUG(SL_DBG_FDSETS, sldbg_print_fdsets(" pre select", sel_set)) ;
	SLDEBUG(SL_DBG_TIMERS, sldbg_print_seltvp("sel timer", tvptr)) ;
	SLDEBUG(SL_DBG_TIMERS, sldbg_print_timerlist("sel tlist", timerlist)) ;
    RESTART_AFTER_EINTR:
        selret = select(max_fd + 1,
                        &sel_set[0], &sel_set[1], &sel_set[2],
                        tvptr) ;
	SLDEBUG(SL_DBG_FDSETS, sldbg_print_fdsets("post select", sel_set)) ;
        if (selret == -1) {
            if (errno == EINTR) {
                goto RESTART_AFTER_EINTR ;
            } else {
                return -1 ;
            }
        }

        /* Call timers. Timers are handled first, because they are,
	 * obviously, time-critical. */
    RESTART_AFTER_CHANGED_TIMER:
	timer_changed = 0 ;
        while (timerlist && timer_now() >= timerlist->when) {
            sltimer_t *timer = timerlist ;

	    /* Take timer out of list. */
            timerlist = timerlist->next ;
            if (timerlist) {
                timerlist->previousp = &timerlist ;
            }

	    /* Call timer if it is active. */
            if (timer->active) {
		/* Identify current timer to sl_remove_timer(). */
                current_timer = timer ;
                timer->handler(timer, timer->context) ;
		if (terminate_argument < 0) {
		    goto BAIL_OUT ;
		}
                current_timer = 0 ;
            }
	    
            /* Timer may have removed itself, but if not, re-insert
	     * it. */
            if (timer->repeat && timer->in_use) {
                timer->when += timer->repeat ;
                insert_timer(timer) ;
            } else {
		free_timer(timer) ;
	    }
		

	    /* The timer may have changed the timer list. */
	    if (timer_changed) {
		goto RESTART_AFTER_CHANGED_TIMER ;
	    }

        }

        /* Call fd handlers. */
        if (selret > 0) {
            int cond ;          /* Condition 0..2. */
            int fd ;            /* File descriptor. */

            for (cond = 2; cond >= 0; cond--) {
                for (fd = 0; fd <= max_fd; fd++) {
                    fdh_t *fdh = &fd_handler[fd][cond] ;

                    if   (fdh->in_use && fdh->active
                          && FD_ISSET(fd, &sel_set[cond])) {
                        fdh->handler(fd, fdh->context) ;
			if (terminate_argument < 0) {
			    goto BAIL_OUT ;
			}
                    }
                }
            }
        }
    } while (!loop_terminated) ;

 BAIL_OUT:
    return terminate_argument ;
}

/** Terminate the select loop. If the argument is negative, the select
 * loop will be terminated immediately; otherwise all outstanding
 * events are handled before the loop is terminated. sl_run_loop()
 * will return the argument of sl_terminate_loop().
 */
void sl_terminate_loop(int argument)
{
    SLDEBUG_FUNCTION(("sl_terminate_loop(%d)", argument)) ;
    loop_terminated = 1 ;
    terminate_argument = argument ;
}

/* EOF */
