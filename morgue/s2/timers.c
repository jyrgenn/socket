/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: timers.c 8 2005-05-18 14:40:31Z ni $
 *
 * timer handlers for Socket version 2.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <errno.h>

#include "bsd_sysexits.h"
#include "socket2.h"
#include "selectloop.h"

char timers_ident[] = "$Id: timers.c 8 2005-05-18 14:40:31Z ni $" ;

sltimer_t *recv_timer ;


static void handle_receive_timeout(sltimer_t *timer, void *context)
{
    DEBUG_FUNCTION(("handle_receive_timeout()")) ;
    err_printf("receive timer expired") ;
    exit(EX_TEMPFAIL) ;
}


void reset_receive_timeout(void)
{
    DEBUG_FUNCTION(("reset_receive_timeout()")) ;
    if (options.receive_timeout) {
	if (sl_reset_timer(recv_timer)) {
	    syserr_exit(EX_SOFTWARE, "invalid receive timer") ;
	}
    }
}

void install_timers(void)
{
    DEBUG_FUNCTION(("install_timers()")) ;
    if (options.receive_timeout) {
	recv_timer =
	    sl_install_timer((LONGLONG) options.receive_timeout * 1000,
			     0, 0, handle_receive_timeout, 0, 0) ;
	if (recv_timer == NULL) {
	    syserr_exit(EX_SOFTWARE, "cannot install receive timer") ;
	}
    }
}

/* EOF */
