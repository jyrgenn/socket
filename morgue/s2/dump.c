/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: dump.c 17 2007-07-11 10:00:16Z ni $
 *
 * Packet logging for Socket version 2.
 *
 */

char dump_ident[] = "$Id: dump.c 17 2007-07-11 10:00:16Z ni $" ;

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include "bsd_sysexits.h"
#include "socket2.h"

#include <assert.h>		/* *After* socket2.h for NDEBUG definition. */

#define IN_PREFIX  "-> "	/* Prefix for incoming packets. */
#define OUT_PREFIX "<- "	/* Prefix for outgoing packets. */
#define NCHARS 16		/* Number of characters to print per line. */

#define XPRINT2(c)                                              \
    do {                                                        \
        putc("0123456789abcdef"[((c) >> 4) & 0xf], diag_out) ;  \
        putc("0123456789abcdef"[(c) & 0xf], diag_out) ;         \
    } while (0)

#define NHALF (NCHARS / 2)
#define NHLIM ((NCHARS - 1) / 2)


/** Print a text dump of a data packet.
 */
void text_dump_packet(char *packet, int length, int incoming)
{
    char *prefix = incoming ? IN_PREFIX : OUT_PREFIX ;
    int bol = 1 ;		/* Are we at the beginning of the line? */
    
    DEBUG_FUNCTION(("text_dump_packet(%p, %d, %d)",
		    packet, length, incoming)) ;
    while (length--) {
	if (bol) {
	    fputs(prefix, diag_out) ;
	}
	putc(*packet, diag_out) ;
	bol = *packet == '\n' ;
	packet++ ;
    }
}


/** Print a hexdump of a data packet. Dumping packets must have been
 * initialized with init_packetdump().
 *
 * @param packet      pointer to the packet to dump
 * @param length      length of the packet
 * @param incoming    non-zero iff packet was read from the socket
 */
void hexdump_packet(char *packet, int length, int incoming)
{
    char *prefix = incoming ? IN_PREFIX : OUT_PREFIX ;
    int n_chars ;		/* Number of chars to print. */
    int i ;			/* Inner loop counter. */
    int address = 0 ;		/* Address to print in beginning of line. */
    
    DEBUG_FUNCTION(("hexdump_packet(%p, %d, %d)", packet, length, incoming)) ;
    while (length > 0) {
	n_chars = MIN(length, NCHARS) ;
	fprintf(diag_out, "%s%08x: ", prefix, address) ;
	for (i = 0; i < n_chars; i++) {
	    XPRINT2(packet[i]) ;
	    putc(i == NHLIM ? '-' : ' ', diag_out) ;
	}
	for ( ; i < NCHARS; i++) {
	    fprintf(diag_out, "  ") ;
	    putc(i == NHLIM ? '-' : ' ', diag_out) ;
	}
	fprintf(diag_out, "\"") ;
	for (i = 0; i < n_chars; i++) {
	    if (isprint((unsigned char) packet[i])) {
		putc(packet[i], diag_out) ;
	    } else {
		putc('.', diag_out) ;
	    }
	}
	fprintf(diag_out, "\"\n") ;
	address += n_chars ;
	packet += n_chars ;
	length -= n_chars ;
    }
}
      

/* EOF */
