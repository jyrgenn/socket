/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: options.c 17 2007-07-11 10:00:16Z ni $
 *
 * Option parsing for Socket version 2.
 *
 */

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "bsd_sysexits.h"
#include "socket2.h"

char options_ident[] = "$Id: options.c 17 2007-07-11 10:00:16Z ni $" ;

struct OPTIONS options ;        /* Struct type declared in socket.h. */

typedef enum { BOOL, INT, OINT, STRING } option_type_t ;

typedef struct OPTION_DESCRIPTOR {
    char optchar ;              /* Option character. */
    const char *arg ;           /* Name of option argument or null. */
    option_type_t type ;        /* Data type of the option. */
    const char *description ;   /* Descriptive string for usage(). */
    /* Variable to set and default value must be kept separate for
     * BOOL/INT and STRING. Oh well. This is C. */
    int *var ;                  /* Variable to set for BOOL and INT.
                                 * Default value for BOOL is always
                                 * false; an INT without arg is
                                 * incremented on each appearance. */
    int intdefault ;            /* Default value of the variable for
                                 * INTs with arguments. */
    char **stringvar ;          /* Variable to set for STRING. The
                                 * default is null. */
} opt_desc_t ;

opt_desc_t option_descriptor[] = {
    { '0', 0, BOOL, "use zero-sized packet as EOF (useful with UDP only)",
      &options.zero_packet, 0, 0 },
    { 'B', "buffer-size", INT, "size of I/O buffers in bytes",
      &options.buffersize, DEFAULT_BUFSIZE, 0 },
    { 'C', 0, BOOL, "count the amount of data transferred",
      &options.count_data, 0, 0 },
    { 'D', "output-file", STRING, "redirect diagnostic output to file",
      0, 0, &options.diag_file },
    { 'H', 0, BOOL, "don't exit on (half-)close by peer",
      &options.survive_halfclose, 0, 0 },
    { 'L', "time-to-live", INT, "set time-to-live for IP datagrams",
      &options.ttl, DEFAULT_TTL, 0 },
    { 'N', 0, BOOL, "non-blocking mode", &options.non_blocking, 0, 0 },
    { 'P', 0, BOOL, "connect program directly to socket fd (don't use pipes)",
      &options.dont_pipe, 0, 0 },
    { 'Q', "queue-length", INT, "listen queue length",
      &options.queue_length, DEFAULT_QLENGTH, 0 },
    { 'R', 0, BOOL, "don't set SO_REUSEADDR", &options.no_reuseaddress, 0, 0 },
    { 'S', 0, BOOL, "use SOCK_SEQPACKET", &options.sock_seqpacket, 0, 0 },
    { 'T', "timeout", INT, "receive timeout (milliseconds)",
      &options.receive_timeout, 0, 0 },
    { 'U', 0, BOOL, "use Unix domain socket", &options.socktype_unix, 0, 0 },
    { 'V', 0, BOOL, "print version", &options.print_version, 0, 0 },
    { 'a', "local-address", STRING, "bind socket to specified local address",
      0, 0, &options.local_address },
    { 'b', 0, BOOL, "run as background daemon", &options.background, 0, 0 },
    { 'c', 0, BOOL, "translate CR/LF <-> LF", &options.translate_lf, 0, 0 },
    { 'd', 0, BOOL, "dump each packet to diagnostic output",
      &options.hex_dump, 0, 0 },
    { 'f', 0, BOOL, "fork handler process", &options.fork_handler, 0, 0 },
#ifdef S2_DEBUG
    { 'g', "debug-value", INT, "set debug flags according to value",
      &options.debug, 0, 0 },
#endif /* S2_DEBUG */
    { 'h', 0, INT, "print usage message and exit; use twice to read manpage",
      &options.usage, 0, 0 },
    { 'l', 0, BOOL, "loop to create/accept other connections",
      &options.loop, 0, 0 },
    { 'm', "mode", OINT, "access mode for UNIX domain server socket",
      &options.unix_mode, DEFAULT_ACCMODE, 0 },
    { 'n', 0, BOOL, "don't look up names", &options.nolookups, 0, 0, },
    { 'p', "program", STRING, "run program connected to socket", 0, 0,
      &options.run_program },
    { 'q', 0, BOOL, "don't quit on EOF on stdin", &options.quit_on_eof, 1, 0 },
    { 'r', 0, BOOL, "read from socket only", &options.read_only, 0, 0 },
    { 's', 0, BOOL, "create server socket", &options.server_mode, 0, 0 },
    { 't', 0, BOOL, "print each packet as text to diagnostic output",
      &options.text_dump, 0, 0 },
    { 'u', 0, BOOL, "use UDP socket", &options.socktype_udp, 0, 0 },
    { 'v', 0, INT, "verbosity level (number of \"v\"s)",
      &options.verbosity, 1, 0 },
    { 'w', 0, BOOL, "write to socket only", &options.write_only, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 0 } } ;



/** Print a usage message and exit with ERREXIT_USAGE. If `format' is
 * non-zero, print additional message before. If `verbose' is
 * non-zero, print detailed option descriptions and exit with zero.
 *
 * @param verbose if non-zero, print option descriptions and exit with zero
 * @param printf-like format string
 * @param (optional format string arguments)
 */
void usage_exit(int verbose, char *format, ...)
{
    va_list arglist ;
    opt_desc_t *odp ;           /* Options descriptor pointer. */
    FILE *out = stdout ;        /* Channel to print usage message to. */
    int first_col_width = 0 ;   /* Max. width of first column. */

    DEBUG_FUNCTION(("usage_exit(%d, %s, ...)", verbose, format)) ;
    if (format) {               /* If there is a format spec, we
                                 * have encountered an error
                                 * condition -> write to diag_out. */
        out = diag_out ;
        va_start(arglist, format) ;
        fprintf(out, PROGRAM_NAME ": ") ;
        vfprintf(out, format, arglist) ;
        fprintf(out, "\n") ;
    }
    fprintf(out, "Usage: " PROGRAM_NAME " [options] host:port\n") ;
    fprintf(out, "       " PROGRAM_NAME " -s [options] [address]:port\n") ;
    fprintf(out, "       " PROGRAM_NAME " -[s]U [options] pathname\n") ;

    if (verbose) {
        /* This is only the case if explicitly requested. */
        fprintf(out, "Options:\n") ;
        /* First check how wide the first column can get at most. */
        for (odp = option_descriptor; odp->optchar; odp++) {
            first_col_width = MAX(first_col_width,
                                  odp->arg ? strlen(odp->arg) : 0) ;
        }       
        DEBUG(S2_DBG_OPTCOLS,
              fprintf(s2_debug_stream,
                      "first_col_width: %d\n", first_col_width));
        for (odp = option_descriptor; odp->optchar; odp++) {
            if (odp->arg) {
                switch (odp->type) {
                  case OINT:
                    fprintf(out, " -%c %s:%*s %s (default: 0%03o)\n",
                            odp->optchar, odp->arg,
                            first_col_width - (int) strlen(odp->arg), "",
                            odp->description, odp->intdefault) ;
                                                    break ;
                  case INT:
                    fprintf(out, " -%c %s:%*s %s (default: %d)\n",
                            odp->optchar, odp->arg,
                            first_col_width - (int) strlen(odp->arg), "",
                            odp->description, odp->intdefault) ;
                                                    break ;
                  case STRING:
                    fprintf(out, " -%c %s:%*s %s\n", odp->optchar,
                            odp->arg, first_col_width - (int) strlen(odp->arg),
                            "", odp->description) ;
                    break ;
                  default:
                    abort() ;
                }
            } else {
                fprintf(out, " -%c:%*s  %s\n", odp->optchar,
                        first_col_width, "", odp->description) ;
            }
        }
    } else {
        fprintf(out, "use \"-h\" option for detailed option descriptions\n") ;
    }
    exit(format ? EX_USAGE : 0) ;
}


void parse_options(int *pargc, char ***pargv)
{
    /* We need space for at most all option characters plus (at most)
     * a colon for each and a terminating null byte. */
    char optstring[2 * sizeof(option_descriptor)
                   / sizeof(opt_desc_t) + 1] ;
    opt_desc_t *od_table[256] ;
    char *optptr ;
    int optchar ;
    opt_desc_t *odp ;
    int i ; 

    DEBUG_FUNCTION(("parse_options(%p, %p)", pargc, pargv)) ;
    /* Clear option descriptor table. */
    for (i = 0; i < 255; i++) od_table[i] = 0 ;
    
    /* Build option string for getopt and fill table. */
    optptr = optstring ;
    for (odp = &option_descriptor[0]; odp->optchar != 0; odp++) {
        *optptr++ = odp->optchar ;
        if (odp->arg) {
            *optptr++ = ':' ;
        }
        od_table[(unsigned int) odp->optchar] = odp ;
        if (odp->type != STRING) {
            *odp->var = odp->intdefault ;
        }
    }
    *optptr = 0 ;

    while ((optchar = getopt(*pargc, *pargv, optstring)) != -1) {
        if (optchar == '?') {
            optchar = 'h' ;
        }
        odp = od_table[(unsigned int) optchar] ;
        if (odp == 0) {
            usage_exit(0, "invalid option '%c'", optchar) ;
        }
        switch (odp->type) {
          case BOOL:
            *odp->var = 1 ;
            break ;
          case INT:
          case OINT:
            if (optarg) {
                *odp->var = (int) strtol(optarg, 0, 0) ;
            } else {
                (*odp->var)++ ;
            }
            break ;
          case STRING:
            if (*odp->stringvar) {
                usage_exit(0, "option '%c' set twice", optchar) ;
            }
            *odp->stringvar = optarg ;
            break ;
        }
    }
    *pargc -= optind ;
    *pargv += optind ;
}

/* EOF */
