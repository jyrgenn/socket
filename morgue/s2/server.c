/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: server.c 17 2007-07-11 10:00:16Z ni $
 *
 * Server code for Socket version 2.
 *
 */

#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>

#include "bsd_sysexits.h"
#include "socket2.h"

char server_ident[] = "$Id: server.c 17 2007-07-11 10:00:16Z ni $" ;

/** Create a server socket of the specified type and bind it to the
 * specified address.
 * 
 * @param socktype   The `type' argument to socket().
 * @param sap        Pointer to socket address struct.
 * @param sa_size    Size of the socket address.
 * @return           The socket file descriptor or -1 on failure.
 */
int bind_server_socket(int socktype, struct sockaddr *sap, int sa_size)
{
    int sockfd ;                /* Socket file descriptor. */
    int one = 1 ;               /* setsockopt() wants a variable. */
    
    DEBUG_FUNCTION(("bind_server_socket(%d, %p, %d)",
                    socktype, sap, sa_size)) ;
    if ((sockfd = socket(sap->sa_family, socktype, 0)) < 0) {
        return -1 ;
    }

    if (!options.no_reuseaddress) {
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *) &one,
                       sizeof(one)) < 0) {
            return -1 ;
        }
    }

    verbose(VERBOSE_CONNECT, "binding socket to %s... ", sockaddr_string(sap)) ;
    if (bind(sockfd, sap, sa_size) == -1) {
        verbose(VERBOSE_CONNECT, "\n") ;
        close(sockfd) ;
        return -1 ;
    }
    
    verbose(VERBOSE_CONNECT, "done\n") ;
    return sockfd ;
}

/** Do server functions for datagram socket.
 * 
 * @param sap        Pointer to socket address struct.
 * @param sa_size    Size of the socket address.
 */
static void server_dgram(struct sockaddr *sap, int sa_size)
{
#if 0                           /* NYI */
    int sockfd ;                /* Socket file descriptor. */

    DEBUG_FUNCTION(("server_dgram(%p, %d)", sap, sa_size)) ;
    sockfd = bind_server_socket(SOCK_DGRAM, sap, sa_size) ;
    if (sockfd == -1) {
        syserr_exit(EX_OSERR, "cannot bind server socket") ;
    }

    do {
        struct sockaddr_in sin ;
        struct iovec iov = { inbuffer, sizeof(inbuffer) } ;
        struct msghdr msg = { (caddr_t) &sin, sizeof(sin), &iov, 1, 0, 0, 0 } ;

        memset(&sin, 0, sizeof(sin)) ;
        read_ret = recvmsg(udpsock, &msg, 0) ;
        if (read_ret == -1) {
            syserr_exit(EX_OSERR, "socket read error") ;
        }

        verbose(VERBOSE_PACKET, "received %d bytes from %s\n",
                read_ret, inet_ntoa(sin.sin_addr)) ;
        }                       
        if (read_ret > 0) {
            if (write(STDOUT_FILENO, inbuffer, read_ret) == -1) {
                fprintf(diag_out, "%s: write error (%s)\n",
                        progname, strerror(errno)) ;
                exit(EX_OSERR) ;
            }
        }
    } while (read_ret > 0 || !zero_end) ;
#endif /* 0 */
}


/** Do server functions for stream socket. (And seqpacket, too.)
 * 
 * @param sap        Pointer to socket address struct.
 * @param sa_size    Size of the socket address.
 */
static void server_stream(struct sockaddr *sap, int sa_size)
{
    int sockfd ;                /* Socket file descriptor for accept(2). */
    int connected_socket ;      /* Socket file descriptor of the
                                 * active connection.  */
    int socktype = options.sock_seqpacket ? SOCK_SEQPACKET : SOCK_STREAM;

    DEBUG_FUNCTION(("server_stream(%p, %d)", sap, sa_size)) ;
    sockfd = bind_server_socket(socktype, sap, sa_size) ;
    if (sockfd == -1) {
        syserr_exit(EX_OSERR, "cannot bind server socket") ;
    }
    if (listen(sockfd, options.queue_length) == -1) {
        syserr_exit(EX_OSERR, "cannot listen on socket") ;
    }

    /* server loop */
    do {
        struct sockaddr saddr ;
        unsigned int alen = sizeof(saddr) ;
        int in_fd = STDIN_FILENO ;
        int out_fd = STDOUT_FILENO ;

        /* accept a connection */
        if ((connected_socket = accept(sockfd, (struct sockaddr *) &saddr,
                                       &alen)) == -1) {
            syserr_exit(EX_OSERR, "accept failed") ;
        }
        verbose(VERBOSE_CONNECT, "connection from %s\n",
                sockaddr_string(&saddr)) ;
        if (options.fork_handler) {
            int childpid ;      /* Process ID of the child process. */
            
            childpid = fork() ;
            switch (childpid) {
              case 0:
                exit_dontflush = TRUE ;
                /* Call child program here etc. (and if there is
                 * none?) */
                err_exit(EX_UNAVAILABLE, "child process not yet implemented") ;
                break ;
              case -1:
                syserr_printf("fork failed") ;
                return ;
              default:
                verbose(VERBOSE_CHILD, "child process %d forked\n", childpid) ;
                close(connected_socket) ;
                return ;
            }
        } else {
            if (options.run_program) {
                connect_child_proc(options.run_program, connected_socket,
                                   &out_fd, &in_fd) ;
            }
            install_stream_handlers(connected_socket, in_fd, out_fd) ;
        }
    } while (options.loop) ;
}



void server_unix(char *pathname)
{
    static struct sockaddr_un sa_un ;
    int sun_len = strlen(pathname) + 1 +
            offsetof(struct sockaddr_un, sun_path) ;

    DEBUG_FUNCTION(("server_unix(%s)", pathname)) ;
    if (strlen(pathname) >= 104) {
        err_exit(EX_DATAERR, "socket pathname too long") ;
    }
    
    memset(&sa_un, 0, sizeof(sa_un)) ;
    sa_un.sun_family = AF_LOCAL ;
    strncpy(sa_un.sun_path, pathname, sizeof(sa_un.sun_path) -1) ;

    server_stream((struct sockaddr *) &sa_un, sun_len) ;
}

void server_ip(char *host_port)
{
    static struct sockaddr_in s_in ;
    char *service ;

    DEBUG_FUNCTION(("server_ip(%s)", host_port)) ;
    switch (resolve_host_port(host_port, &s_in, &service,
                              options.socktype_udp ? "udp" : "tcp")) {
      case RESERR_INVALID:
        err_exit(EX_DATAERR, "invalid host:port argument \"%s\"",
                 host_port) ;
        break ;
      case RESERR_PORT:
        err_exit(EX_DATAERR, "cannot resolve service \"%s\"",
                 service) ;
        break ;
      case RESERR_HOST:
        err_exit(EX_NOHOST, "address \"%s\" not found (%s)",
                 host_port, hstrerror(h_errno)) ;
        break ;
    }

    s_in.sin_family = PF_INET ;
    if (options.socktype_udp) {
        server_dgram((struct sockaddr *) &s_in, sizeof(s_in)) ;
    } else {
        server_stream((struct sockaddr *) &s_in, sizeof(s_in)) ;
    }
    /* Now fall back to main, where the select loop is called. */
}

/* EOF */
