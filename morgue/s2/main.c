/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: main.c 8 2005-05-18 14:40:31Z ni $
 *
 * main() for Socket version 2.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#define MAIN_C
#include "socket2.h"
#include "selectloop.h"


char main_ident[] = "$Id: main.c 8 2005-05-18 14:40:31Z ni $" ;


int main(int argc, char *argv[])
{
    DEBUG_INITIALIZE() ;

    DEBUG_FUNCTION(("main(%d, %p)", argc, argv)) ;
    /* parse_options() will shift argv such that the first argument is
     * in argv[0]. */
    parse_options(&argc, &argv) ;

    if (options.print_version) {
	print_version() ;
	exit(0) ;
    }

    if (options.usage > 1) {
	puts(manpage) ;
	exit(0) ;
    }
    if (options.usage) {
        usage_exit(1, 0) ;
    }

    if (options.socktype_udp && options.socktype_unix) {
        usage_exit(0, "use only one of -u and -U") ;
    }

    if (options.read_only && options.write_only) {
        usage_exit(0, "use only one of -r and -w") ;
    }

    if (options.loop && ! options.server_mode) {
        usage_exit(0, "looping makes only sense for server mode") ;
    }

    if (options.background && ! options.server_mode) {
        usage_exit(0, "background makes only sense for server mode") ;
    }

    if (options.fork_handler && ! options.server_mode) {
        usage_exit(0, "forking makes only sense for server mode") ;
    }

    if (options.diag_file) {
	if ((diag_out = fopen(options.diag_file, "w")) == NULL) {
	    syserr_exit(EX_CANTCREAT, "cannot open diagnostic output \"%s\"",
			options.diag_file) ;
	}
    } else {
	diag_out = stderr ;
    }

    if (options.server_mode) {
        if (options.socktype_unix) {
            if (argc != 1) {
                usage_exit(0, "argument for Unix domain server: pathname") ;
            }
            server_unix(argv[0]) ;
        } else {
            if (argc != 1) {
                usage_exit(0, "argument for %s server: [address:]port",
                           options.socktype_udp ? "UDP" : "TCP") ;
            }
            server_ip(argv[0]) ;
        }
    } else {
        if (options.socktype_unix) {
            if (argc != 1) {
                usage_exit(0, "argument for Unix domain client: pathname") ;
            }
            client_unix(argv[0]) ;
        } else {
            if (argc != 1) {
                usage_exit(0, "argument for %s client: host:port",
                           options.socktype_udp ? "UDP" : "TCP") ;
            }
            client_ip(argv[0]) ;
        }
    }
    install_timers() ;
    sl_run_loop() ;

    return 0 ;
}

/* EOF */
