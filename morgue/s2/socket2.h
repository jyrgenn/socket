/*-
 * Copyright (c) 2001 - 2004 Juergen Nickelsen <ni@jnickelsen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, HAIR, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: socket2.h 17 2007-07-11 10:00:16Z ni $
 *
 * Global definitions and declarations for Socket version 2.
 *
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#ifdef MAIN_C
char socket2_h_ident[] = "$Id: socket2.h 17 2007-07-11 10:00:16Z ni $" ;
#endif /* MAIN_C */

#ifndef LONGLONG
#define LONGLONG long long
#endif /* LONGLONG */

#ifndef PROGRAM_NAME            /* Allow override by the CFLAGS. */
#define PROGRAM_NAME "socket"
#endif /* PROGRAM_NAME */

/* Maximum hostname length as defined by RFC 1034 (255), plus some
 * safety margin. */
#define MAX_HOSTNAME_LEN 260

/* Maximum service name length. This is more like guesswork. I
 * haven't seen any name longer than 15 characters. */
#define MAX_SERVICENAME_LEN 20


/* Maximum size of UDP payload: 2^16 - 1 - IP Header - UDP header. */
#define MAX_UDP_PAYLOAD ((1 << 16) - 1 - 20 - 8)

/* Default values for options.
 */
#define DEFAULT_BUFSIZE 65536   /* I/O buffer size. */
#define DEFAULT_TTL       255   /* time to live. */
#define DEFAULT_QLENGTH     5   /* listen queue length. */
#define DEFAULT_ACCMODE  0600   /* access mode for Unix domain socket. */

/* Names of environment variables for child process. */
#define ENVNAME_OWNADDR  "SOCKET_LOCAL_ADDRESS"
#define ENVNAME_OWNPORT  "SOCKET_LOCAL_PORT"
#define ENVNAME_PEERADDR "SOCKET_PEER_ADDRESS"
#define ENVNAME_PEERPORT "SOCKET_PEER_PORT"

#ifndef HAVE_OFFSETOF
#define offsetof(type, member)  (int)(&(((type *) 0)->member))
#endif /* HAVE_OFFSETOF */

#undef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a, b) ((a) > (b) ? (b) : (a))

#ifdef TRUE
#undef TRUE                     /* Don't use other definitions */
#endif /* TRUE */
#ifdef FALSE
#undef FALSE
#endif /* FALSE */

#define TRUE  (1)
#define FALSE (0)


/* verbosity values. */
typedef enum {
    VERBOSE_NONE,               /* Errors only. */
    VERBOSE_CONNECT,            /* Connections. */
    VERBOSE_NET,                /* Name lookup, EOF, etc. */
    VERBOSE_CHILD,              /* Events concerning child processes. */
    VERBOSE_MSGS,               /* Network payload messages.  */
    VERBOSE_MAX
} verbosity_t ;


/* Error codes for the resolver module. */
#define RESERR_OK       0
#define RESERR_HOST    -1
#define RESERR_PORT    -2
#define RESERR_INVALID -3


#ifdef S2_DEBUG

extern FILE *s2_debug_stream ;

#define S2_DBG_IOCALLS  0x00000001 /* I/O call results. */
#define S2_DBG_FDVALS   0x00000002 /* File descriptor values. */
#define S2_DBG_OPTCOLS  0x00000004 /* Option width columns. */
#define S2_DBG_FUNCTION 0x00000008 /* Function calls. */
#define S2_DBG_SYSERRS  0x00000010 /* Transient system errors. */

#define DEBUG(flag, code)                \
    if (options.debug & (flag)) {       \
        code ;                           \
        fprintf(s2_debug_stream, "\n") ; \
    }


#define DEBUG_FUNCTION(func) \
    DEBUG(S2_DBG_FUNCTION, s2_debug_printf func)
#define DEBUG_INITIALIZE s2dbg_initialize

void s2_debug_printf(const char *format, ...) ;
void s2dbg_initalize(void) ;


#else  /* S2_DEBUG */
#define DEBUG(a, b)
#define DEBUG_FUNCTION(func)
#define NDEBUG
#endif /* S2_DEBUG */

/* See options.c for a description of the options.  */
extern struct OPTIONS {
    int usage ;
    int zero_packet ;
    int buffersize ;
    int translate_lf ;
    int non_blocking ;
    int server_mode ;
    int background ;
    int fork_handler ;
    int loop ;
    int unix_mode ;
    char *run_program ;
    int quit_on_eof ;
    int read_only ;
    int verbosity ;
    int write_only ;
    int print_version ;
    int receive_timeout ;
    int socktype_udp ;
    int socktype_unix ;
    int sock_seqpacket ;
    int no_reuseaddress ;
    int ttl ;
    int dont_pipe ;
    int count_data ;
    int survive_halfclose ;
    char *local_address ;
    int queue_length ;
    int nolookups ;
    int hex_dump ;
    int text_dump ;
    char *diag_file ;
    int debug ;
} options ;

extern char *manpage ;
extern int exit_dontflush ;     /* Don't flush buffers on exit (set
                                 * in child process). */

extern FILE *diag_out ;         /* Stream for diagnostic output. */


void usage_exit(int verbose, char *format, ...)
#ifdef __gcc_compiled__
     __attribute__ ((format (printf, 2, 3)))
#endif /* __gcc_compiled__ */
     ;

/** Print a version id string. */
void print_version(void) ;
     
     
/** Write a message to standard error depending on the verbosity level.
 * 
 * @param level    Needed verbosity level for message.
 * @param format   Printf-like format string.
 * @param (format string arguments)
 */
void verbose(int level, char *format, ...) ;


/* Strip CR characters in `src', copying to `dest'.
 *
 * @param src     Pointer to data source.
 * @param amount  Amount of chars at src.
 * @param dest    Pointer to data destination.
 * @return        Number of characters at dest.
 */
int strip_crs(char *src, int amount, char *dest) ;


/* Expand LF characters in `src' to CRLF in `dest'.
 *
 * @param src     Pointer to data source.
 * @param amount  Amount of chars at src.
 * @param dest    Pointer to data destination.
 * @return        Number of characters at dest.
 */
int insert_crs(char *src, int amount, char *dest) ;


/** Install the I/O handler functions for the socket and initialize
 * the I/O buffers.
 * 
 * @param sockfd   socket file descriptor
 * @param in_fd    input file descriptor to read from
 * @param out_fd   output file descriptor to write to
 */
void install_stream_handlers(int sockfd, int in_fd, int out_fd) ;


/** Install the I/O handler functions for the socket and initialize
 * the I/O buffers.
 * 
 * @param sockfd   socket file descriptor
 * @param in_fd    input file descriptor to read from
 * @param out_fd   output file descriptor to write to
 */
void install_dgram_handlers(int sockfd, int in_fd, int out_fd) ;


/** Allocate memory; exit on failure.
 * 
 * @param nbytes    Amount of memory to allocate.
 * @param reason    Object of allocation (for informational purposes).
 * @return          Pointer to allocated area.
 */
void *ck_malloc(int nbytes, char *reason) ;


/** Duplicate a string; exit on failure.
 *
 * @param string    The string to duplicate.
 * @param reason    Object of allocation (for informational purposes).
 * @return          Pointer to copy of string.
 */
char *ck_strdup(char *string, char *reason) ;


/** Resolve a hostname to an IP address and write the result to the
 * sockaddr_in passed. The hostname may also be an IP address in
 * dotted decimal notation.
 * 
 * @param hostnamep  Hostname string.
 * @param sinp       Pointer to sockaddr_in.
 * @return           Non-zero if the hostname could be resolved, zero else.
 */
int resolve_hostname(char *hostname, struct sockaddr_in *sinp) ;


/* Resolve the service name in *`namep' for `protocol' and return
 * the port number in host byte order. If the argument was a decimal
 * number, return the number.
 *
 * @param name      service name string.
 * @param protocol  "tcp" or "udp"
 * @return          Non-zero if the port could be resolved, zero else.
 */
int resolve_service(char *name, char *protocol) ;


/** Resolve host and service give in the form "host:service" to an
 * sockaddr_in struct. Host and service may also be specified
 * numerically, i. e. as a dotted decimal IP address and a port
 * number. IP address and port number are written into the
 * sockaddr_in struct supplied by the caller.
 *
 * @param host_port   string with host and port specifier
 * @param sin_p       socket address struct to carry result
 * @param service_p   pointer to service variable; may be NULL
 * @param protocol    "tcp" or "udp"
 * @return            zero iff successful
 */
int resolve_host_port(char *host_port, struct sockaddr_in *sin_p,
                      char **service_p, char *protocol) ;


/** Build a printable representation from a sockaddr struct and
 * return it. Lookup hostname and service name, if applicable and
 * possible. The returned string is static and will be overwritten on
 * the next call.
 *
 * @param saddr    the socket address struct
 * @return a static string with the printable representation
 */
char *sockaddr_string(struct sockaddr *saddr) ;


/** Print a message about a system error (with help of strerror()).
 * 
 * @param format  Format string.
 * @param (optional format arguments)
 */
void syserr_printf(const char *format, ...)
#ifdef __gcc_compiled__
     __attribute__ ((format (printf, 1, 2)))
#endif /* __gcc_compiled__ */
     ;


/** Print a message about a system error (with help of strerror()) and
 * exit with the specified value.
 *
 * @param exitcode    from sysexits
 * @param format      format string
 * @param (optional format arguments)
 */
void syserr_exit(int exitcode, char *format, ...)
#ifdef __gcc_compiled__
     __attribute__ ((format (printf, 1, 2)))
#endif /* __gcc_compiled__ */
     ;


/** Print a message about an error.
 *
 * @param format       format string
 * @param (optional format arguments)
 */
void err_printf(char *format, ...)
#ifdef __gcc_compiled__
     __attribute__ ((format (printf, 1, 2)))
#endif /* __gcc_compiled__ */
     ;


/** Print a message about an error and exit with value.
 *
 * @param exitcode     from sysexits
 * @param format       format string
 * @param (optional format arguments)
 */
void err_exit(int exitcode, char *format, ...)
#ifdef __gcc_compiled__
     __attribute__ ((format (printf, 2, 3)))
#endif /* __gcc_compiled__ */
     ;

void parse_options(int *pargc, char ***pargv) ;

void client_unix(char *pathname) ;

void client_ip(char *host_port) ;

void server_unix(char *pathname) ;

void server_ip(char *portname) ;

int is_decimal(char *string) ;


void s2dbg_initialize(void) ;

void s2dbg_print_iocall(char *call, int fd, int result) ;


/** Print a hexdump of a data packet. Dumping packets must have been
 * initialized with init_packetdump().
 *
 * @param packet      pointer to the packet to dump
 * @param length      length of the packet
 * @param incoming    non-zero iff packet was read from the socket
 */
void hexdump_packet(char *packet, int length, int incoming) ;


/** Print a text dump of a data packet.
 *
 * @param packet      pointer to the packet to dump
 * @param length      length of the packet
 * @param incoming    non-zero iff packet was read from the socket
 */
void text_dump_packet(char *packet, int length, int incoming) ;


/** Create a child process running the specified command line and connect its
 * input and outputs to the specified file descriptors. Put information about
 * local and remote address of the sepcified socket into the child's
 * environment (if it is a TCP or UDP socket at all). Return zero if
 * everything is fine, -1 otherwise. Set errno as appropriate.
 * 
 * @param command    command line to be run
 * @param sockfd     socket file descriptor (for getsock/peername())
 * @param in_fd      file descriptor to be connected to program's stdin
 * @param out_fd     file descriptor to be connected to program's stdout/stderr
 * @return -1 in case of an error, zero otherwise
 */
int connect_child_proc(char *command, int sockfd, int *in_fd, int *out_fd) ;


void install_timers(void) ;

void reset_receive_timeout(void) ;


/* EOF */
