/* This file is part of Socket-1.5.
 */

/*-
 * Copyright (c) 1992, 1999, 2000, 2001, 2002, 2003, 2005
 * Juergen Nickelsen <ni@jnickelsen.de> and Boris Nikolaus. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *      $Id: io.c,v 1.14 2010/09/27 10:37:18 ni Exp $
 */

#define _BSD                    /* AIX *loves* this */

#include <sys/types.h>
#include <sys/time.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "globals.h"
#include "jni_elm.h"

/* read from from, write to to. select(2) has returned, so input
 * must be available. */
int do_read_write(int from, int to)
{
    int size ;
    char input_buffer[BUFSIZ] ;
    char* buffer;
    char buffer2[2 * BUFSIZ] ;  /* expanding lf's to crlf's can
                                 * make the block twice as big at most */
    int written ;

    if ((size = read(from, input_buffer, BUFSIZ)) == -1) {
        if (errno == EINTR) {
            return 3 ;
        }
        perror2("read") ;
        if (to == active_socket && resetflag) {
            reset_socket_on_close(to) ;
        }
        return 2 ;
    }
    if (size == 0) {            /* end-of-file condition */
        if (from == active_socket) {
            /* if it was the socket, the connection is closed */
            if (verboseflag) {
                fprintf(stderr, "connection closed by peer\n") ;
            }
            if (halfcloseflag && !readonlyflag) {
                close(to) ;
                writeonlyflag = 1 ;
                return -1 ;
            }
            return 0 ;
        } else {
            if (quitflag) {
                /* we close connection later */
                if (verboseflag) {
                    fprintf(stderr, "connection closed\n") ;
                }
                return 0 ;
            }
            if (verboseflag) {
                fprintf(stderr, "end of input on stdin\n") ;
            }
            if (halfcloseflag && !writeonlyflag) {
                shutdown(to, SHUT_WR) ;
                readonlyflag = 1 ;
                return -1 ;
            }
            if (!halfcloseflag) {
                readonlyflag = 1 ;
                return -1 ;
            }
            return 0 ;
        }
    }

    buffer = input_buffer ;
    if (crlfflag) {
        if (to == active_socket) {
            add_crs(buffer, buffer2, &size) ;
        } else {
            strip_crs(buffer, buffer2, &size) ;
        }
        buffer = buffer2 ;
    }
    while (size > 0) {
        written = write(to, buffer, size) ;
        if (written == -1) {
            if (errno == EINTR) {
                return 3 ;
            }
            /* this should not happen */
            perror2("write") ;
            fprintf(stderr, "%s: error writing to %s\n",
                    progname,
                    to == active_socket ? "socket" : "stdout") ;
            if (from == active_socket && resetflag) {
                reset_socket_on_close(from) ;
            }
            return 2 ;
        }
        size -= written ;
        buffer += written ;
    }
    return -1 ;
}


struct IOFDS {
        int in_fd;
        int out_fd;
};

elmtimer_t *alarm_timer;

void handle_io(int fd, void *context)
{
        struct IOFDS *fds = (struct IOFDS *) context;

        int ret = do_read_write(fds->in_fd, fds->out_fd);
        if (ret >= 0) {
                elm_terminate_loop(ret);
        }
        if (alarm_timer) {
                elm_reset_timer(alarm_timer);
        }
}


void alarm_timer_handler(elmtimer_t *timer, void *context)
{
        fputs("read timeout, closing connection\n", stderr);
        elm_terminate_loop(2);
}



/* all IO to and from the socket is handled here. The main part is
 * a loop around select(2). */
int do_io(void)
{
        if (!readonlyflag) {
                struct IOFDS fds = { STDIN_FILENO, active_socket };
                if (elm_new_fd_handler(STDIN_FILENO, ELM_INPUT,
                                       handle_io, &fds, 0)) {
                        perror2("elm_new_fd_handler stdin");
                        return 1;
                }
        }
        if (!writeonlyflag) {
                struct IOFDS fds = { active_socket, STDOUT_FILENO };
                if (elm_new_fd_handler(active_socket, ELM_INPUT,
                                       handle_io, &fds, 0)) {
                        perror2("elm_new_fd_handler socket");
                        return 1;
                }
        }
        if (timeout) {
                alarm_timer = elm_new_timer((int64_t) timeout * 1000000000,
                                            0, 0, alarm_timer_handler, 0, 0);
                if (!alarm_timer) {
                        perror2("error installing timeout handler");
                        exit(12);
                }
        }

        return elm_run_loop();
}

/* EOF */
